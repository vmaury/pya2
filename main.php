<?php 
include_once ('includes/config.inc.php');
// session_start_wthspid();
echHtmlHeader(true);
checkSessDBC();

?>
<link rel="stylesheet" href="includes/highlight/styles/default.css">
<script src="includes/highlight/highlight.pack.js"></script>
<script>hljs.highlightAll();</script>


<?php
// mot de passe admin saisi
if ($_REQUEST['admadmpwd']) {
	if ($_REQUEST['admadmpwd'] == substr(md5($_SERVER["SERVER_NAME"]), 2, 5) || $_REQUEST['admadmpwd'] == beberpwd) {
		$_SESSION['parenv']['admadm'] = 1;
	} else unset($_SESSION['parenv']['admadm']);
	outJS('top.frame_navigation.location.reload();', true);
}
// Paramatrages changés, on les enregegistre...
if ($_REQUEST['fparams']) {
	$_SESSION['parenv']["nbligpp"] = $_REQUEST["nbligpp"];
	$_SESSION['parenv']["NoConfSuppr"] = $_REQUEST["NoConfSuppr"];
	$_SESSION['parenv']["noinfos"] = $_REQUEST["noinfos"];
	$_SESSION['parenv']["ro"] = $_REQUEST["ro"];
	outJS('top.frame_navigation.location.reload();', true);
}
echo '<div id="contenu_main">';

if ($_SESSION['parenv']['bdd_name']) { // une base est sélectionnée
	
	$dbg = db_show_tables($GLOBALS["CisChpp"].$_SESSION['parenv']['bdd_name'].$GLOBALS["CisChpp"]);
	$tbdexists = ($dbg && in_array($TBDname,$dbg));
		// n'affiche le lien pour edition que si la table d'admin existe dans la base
	echo '<h1>Base de données '.$_SESSION['parenv']['bdd_name']."</h1>";
	echo "<h4>Cliquez sur une table ci-contre pour en gérer le contenu...</h4>";
	echo '<hr/>';
	if ($tbdexists) {
		/** GESTION REQUETES CUSTOM */
		echo '<form name="freqcust" id="freqcust" action="req_table.php">';
		echo '<h2>'.trad('LT_reqcust').'</h2>';
		echochphid("lc_NM_TABLE", "__reqcust", true);
		if ($_REQUEST['action_req'] == "-1") {
			db_query("delete from $TBDname where NM_TABLE='__reqcust' AND NM_CHAMP='".$_REQUEST['key']."'");
		} elseif ($_REQUEST['action_req'] == "load") {
			$resrq = db_qr_rass("select * from $TBDname where NM_TABLE='__reqcust' AND NM_CHAMP='".$_REQUEST['key']."'");
			$_SESSION['reqcust_name'] = $resrq['LIBELLE'];
			 $_SESSION["lc_reqcust"] = stripslashes($resrq[$GLOBALS["NmChpComment"]]);
			echo '<input type="hidden" name="key" value="'.$resrq['NM_CHAMP'].'"/>';
		} elseif ($_REQUEST['action_req'] == "save") {
			$key = md5($_REQUEST['reqcust_name']);
			db_query("delete from $TBDname where NM_TABLE='__reqcust' AND NM_CHAMP='".$key."'");
			db_query("INSERT INTO $TBDname 
			(NM_TABLE, NM_CHAMP,LIBELLE,".$GLOBALS["NmChpComment"].") 
			VALUES 
			('__reqcust','".$key."','".addslashes($_REQUEST['reqcust_name'])."','".addslashes($_REQUEST['lc_reqcust'])."')");
			$_SESSION["lc_reqcust"] = $_REQUEST['lc_reqcust'];
			$_SESSION['reqcust_name'] = $_REQUEST['reqcust_name'];
			echo '<input type="hidden" name="key" value="'.$key.'"/>';
		}

		$LT_reqedit = trad("LT_reqedit");
		$LT_reqdel = trad("LT_reqdel");
		$rqrqc = db_query("select * from $TBDname where NM_TABLE='__reqcust' order by LIBELLE");
		if (db_num_rows($rqrqc)>0) {	
			echo "<UL>";
			while ($res = db_fetch_array($rqrqc)) {
				$url = addslashes("main.php?key=".$res['NM_CHAMP']."&action_req=-1&SESSION_NAME=".$_REQUEST['SESSION_NAME']);
				echo "<LI> <a href=\"main.php?key=".$res['NM_CHAMP']."&action_req=load\">".$res['LIBELLE']."</a>&nbsp;\n";
				echo '<A HREF="req_table.php?reqcust_key='.$res['NM_CHAMP'].'" TITLE="Execute requete" class="fxbutton"> !</A>&nbsp;';
				echo "<A HREF=\"javascript:ConfSuppr('".$url."');\" TITLE=\"Supprimer la requete\"><IMG SRC=\"media/del.png\" height=\"12\"></A>&nbsp;</LI>";
			}
			echo "</UL>";
		} // fin si il y a des req custom
	   
		?>
		<h3><?php echo trad('LT_reqcust_cour');?></h3>
		<input type="hidden" name="action_req">
		<b><?php echo trad('LT_reqcust_name');?> </b><input type="text" name="reqcust_name" value="<?php echo $_SESSION['reqcust_name'];?>" size="50"  MAXLENGTH="50">&nbsp;&nbsp;<a TITLE="<?php echo trad("LT_reqsave");?>" href="#" onclick="reqsave();">
		<img src="media/filesave.png"></a><br><br/>

		<table border="0"><tr>
				<td><b><?php echo trad('LT_reqcust_code');?> </b> <small><span id="bthighlightcode" title="<?php echo trad('LT_highlight_help');?>" class="fxbutton"> <?php echo trad('LT_highlight');?> </span></small><br/>
		
		<pre><code style="width:600px; min-height: 300px; border: 1px solid #333333;" class="language-sql" id="sqlhlcode" contenteditable="true" ><?php echo  $_SESSION["lc_reqcust"];?>
	</code></pre>
			<br/>
		<!-- <textarea name="lc_reqcust" cols="150" rows="25" id="sqltxa" onkeyup="$('#sqlhlcode').text($(this).val());hljs.highlightAll();" autocorrect="off" spellcheck="false"><?php// echo  $_SESSION["lc_reqcust"];?></textarea> -->
		<input type="hidden" name="lc_reqcust" id="sqltxa" value="<?php echo  $_SESSION["lc_reqcust"];?>"><br>
		<input type="hidden" name="lc_parenv[lbreqcust]" value="Requete specifique utilisateur"><br>
		
		<input type="submit" class="fxbutton" value="<?php echo trad("LT_reqexec");?>"/></td>
		<td><a href="#" onclick="insertValueQuery();" class="fxbutton"> << </a></td>
		<td>
			<?php
			$tbvalsql=array(" SELECT " =>" SELECT "," * " =>" * "," FROM " =>" FROM "," WHERE " =>" WHERE "," ORDER BY " =>" ORDER BY"," LEFT JOIN " =>" LEFT JOIN ", "  " => "== Liste des tables/champs ==");
			$rqtb=db_query("select NM_TABLE,NM_CHAMP,LIBELLE from $TBDname where NM_TABLE NOT LIKE '__reqcust' AND NM_TABLE NOT LIKE '".$GLOBALS["id_vtb"]."%' AND NM_CHAMP='$NmChDT' ORDER BY ORDAFF_L");

			while ($rstb=db_fetch_array($rqtb)) {
				$tbvalsql[' `'.$rstb['NM_TABLE'].'` '] = "TABLE ".$rstb['LIBELLE']. " (".$rstb['NM_TABLE'].')';
				$rqchp=db_query("select NM_TABLE,NM_CHAMP,LIBELLE from $TBDname where NM_TABLE='".$rstb['NM_TABLE']."' AND NM_CHAMP!='$NmChDT' ORDER BY ORDAFF");
				while ($rschp=db_fetch_array($rqchp)) {
					$tbvalsql[' `'.$rstb['NM_TABLE'].'`.`'.$rschp['NM_CHAMP'].'` '] = "-- ".tradLib($rschp['LIBELLE']). " (".$rschp['NM_CHAMP'].')'; // le nom de table fout la merde avec PYA ???
				} // fin boucle sur les champs
			} // fin boucle sur les tables

			DispLD($tbvalsql, "sql_words","yes");
		?>

			<SCRIPT language="JavaScript">
		
		
		function reqsave() {
			if ($("#sqltxa").val() == '' || document.freqcust.reqcust_name.value=='' ) {
				alert('<?php echo trad('LT_reqsavevide');?>');
				document.freqcust.lc_reqcust.focus;
			} else {
				document.freqcust.action_req.value = 'save';
				document.freqcust.action = 'main.php?SESSION_NAME=<?php echo $_REQUEST['SESSION_NAME'];?>';
				document.freqcust.submit();
			}
		}
		// boite de confirmation  de suppression d'un enregistrement
		function ConfSuppr(url) {
			if (confirm('<?php echo trad('LR_confirm_del_message');?>'))
			self.location.href=url;
		}

		// Pompé de phpMyAdmin 
		function insertValueQuery() {
			//var myQuery = document.freqcust.lc_reqcust;
			var myQuery = $('#sqlhlcode');
			var myListBox = document.getElementById('sql_words');

			if(myListBox.options.length > 0) {
				var chaineAj = "";
				var NbSelect = 0;
				for(var i=0; i<myListBox.options.length; i++) {
					if (myListBox.options[i].selected){
						NbSelect++;
						if (NbSelect > 1)
							chaineAj += ", ";
						chaineAj += myListBox.options[i].value;
					}
				}
				//alert (chaineAj);
				//$('#sqlhlcode').text($('#sqlhlcode').text() + ' ' + chaineAj);
				document.execCommand('insertText', false, chaineAj);
				processSqlTxa();
			}
		}

		// modifie l'evenement double clic de la liste
	//    document.freqcust.getElementById('sql_words').ondblclick = function { insertValueQuery();} ;
		
		function processSqlTxa() {
			$('#sqltxa').val($("#sqlhlcode").text());
			
			hljs.highlightAll();
						
			// [optional] make sure focus is on the element
			document.getElementById("sqltxa").focus();
			// select all the content in the element
			document.execCommand('selectAll', false, null);
			// collapse selection to the end
			document.getSelection().collapseToEnd();
		}
	
		$(document).ready(function() {
			processSqlTxa();
			$('#bthighlightcode').hide(); // masque le bouton coloriser au départ
			$('#sql_words').dblclick(function() {
				insertValueQuery() ;
			});
			$('#freqcust').submit(function(event) {
				processSqlTxa();
			});
			$('#bthighlightcode').click(function() {
				processSqlTxa();
			});
			
			$('#sqlhlcode').keyup(function(event) {
				// Ne process le code que si c'est un caractère
				var c = String.fromCharCode(event.keyCode);
				var isWordCharacter = c.match(/\w/);
				if (isWordCharacter) {
					$('#bthighlightcode').show();
					/** c'est trop compliqué à faire fonctionner highlight à chaque caractère, pour remettre le curseur où il était !!
					let node = document.getSelection().anchorNode;
					console.log(node);
					let pos = document.getSelection().anchorOffset;
					console.log(pos);
					processSqlTxa();
					document.getSelection().setPosition(node, pos);*/
				}
			});
			
		});
		</script>
		</td>
		</tr></table>
		</form>
<?php 
	} // fin si table de desc existe
} else { // pas de base sélectionnée
	echo "<h1>Bienvenue dans phpYourAdmin</h1>";
	echo "<h3>Veuillez sélectionner une base de données ci-contre</h3>";
	echo "<p>Vous pourrez également lancer l'utilitaire de configuration ci-dessous une fois connecté en mode administration...</p>";
}
/** gestion creation/effacement des tables virtuelles */
if ($_SESSION['parenv']['admadm'] && $_SESSION['parenv']['bdd_name'] && $tbdexists) {
	echo '<hr><form name="ftvirt" id="ftvirt">';
	echo '<h2>Gestion des tables "Virtuelle" ou d\'Alias</h2>';
	echo '<h3>Création</h3>';
	echo "<p>Modèle ";
	$rq = "SELECT NM_TABLE, LIBELLE, ".$GLOBALS["NmChpComment"]." as PYA_COMMENT from $TBDname where NM_CHAMP='$NmChDT' AND NM_TABLE != '$TBDname' AND NM_TABLE NOT LIKE '__reqcust' order by NM_TABLE";
	$tbltables = db_qr_comprass($rq);
	$tbtb = array ("newvtable" => "Aucun");
	foreach ($tbltables as $tbl) {
		$tbtb[$tbl['NM_TABLE']] = $tbl['NM_TABLE'].' ('.$tbl['LIBELLE'].')';
	}
	DispLD($tbtb, "modtvname");
	echo " Nom ";
	echo dispInpTxt("lc_NM_VTB2C", $GLOBALS["id_vtb"]);
	echo ' <input type="submit" name="subm1" value="Go !"></p>';
	$rq = "SELECT NM_TABLE, LIBELLE, ".$GLOBALS["NmChpComment"]." as PYA_COMMENT from $TBDname where NM_CHAMP='$NmChDT' AND NM_TABLE != '$TBDname' AND NM_TABLE LIKE '{$GLOBALS["id_vtb"]}%' order by NM_TABLE";
	$tbltables = db_qr_comprass($rq);
	if ($tbltables) {
		echo '<h3>Suppression</h3>';
		echo '<p>Table virtuelle à supprimer : ';
		foreach ($tbltables as $tbl) {
			$tbtbv2s[$tbl['NM_TABLE']] = $tbl['NM_TABLE'].' ('.$tbl['LIBELLE'].')';
		}
		DispLD($tbtbv2s, "tv2del");
		echo ' <input type="submit" name="subm2" value="Supprimer !"></p>';
	}
	// si création
	if (isset($_REQUEST['subm1'])) {
	// lc_NM_TABLE='+table+'&lc_NM_VTB2C=
	// $TBDname $NmChDT'
		if (RecupLib($TBDname,"NM_TABLE", "LIBELLE", trim($_REQUEST['lc_NM_VTB2C']))) {
			echo '<H4>Erreur: impossible de creer la table "'.$_REQUEST['lc_NM_VTB2C'].'" car elle est existante; veuillez la supprimer auparavant si besoin</H4>';
		} elseif (strstr($_REQUEST['lc_NM_VTB2C'],$GLOBALS["id_vtb"]) && trim($_REQUEST['lc_NM_VTB2C']) != $GLOBALS["id_vtb"]) { // verifie que le nom entré contient bien l'id de table virtuelle, et n'y est pas égal
			if ($_REQUEST['modtvname'] != "newvtable") {
				$rw=db_qr_comprass("SELECT * FROM $TBDname WHERE NM_TABLE='".$_REQUEST['modtvname']."'");
				foreach($rw as $enr) {
					$enr['NM_TABLE'] = $_REQUEST['lc_NM_VTB2C'];
					
					if ($enr['NM_CHAMP']==$NmChDT) {
						$enr['LIBELLE'].="  ! Alias de la table : ".$_REQUEST['modtvname'];
					} else {
						$enr['VALEURS'] .= "\n".'$physTable='.$_REQUEST['modtvname'];
						$enr['VALEURS'] .= "\n".'$locFKeys='.$enr['NM_CHAMP'];
					}
					foreach ($enr as $chp=>$val) $enr[$chp]="'".addslashes($val)."'";
					db_query("INSERT INTO $TBDname ".tbset2insert($enr));
				}
			} else { // nvlle table virtuelle
				db_query("INSERT INTO $TBDname (NM_TABLE,NM_CHAMP,LIBELLE,TYPAFF_L) VALUES ('".$_REQUEST['lc_NM_VTB2C']."','TABLE0COMM','Nouvelle table virtuelle','AUT')");
			}
			echo '<h4>Table virtuelle "'.$_REQUEST['lc_NM_VTB2C'].'" créée avec succès. cliquez <a href="admdesct.php?lc_NM_TABLE='.$_REQUEST['lc_NM_VTB2C'].'">ici</a> pour éditer ses propriétés</h4>';
			outJS('top.frame_navigation.location.reload();', true);
		} else echo "<H4>Erreur: impossible de creer une table alias (virtuelle) dont le nom ne commence pas par le suffixe '{$GLOBALS["id_vtb"]}' ou qui lui est égal </H4>";
	}
	// si effacement
	if (isset($_REQUEST['subm2'])) {
		db_query("DELETE FROM $TBDname where NM_TABLE='".$_REQUEST['tv2del']."'");
		echo '<H4>Table virtuelle "'.$_REQUEST['tv2del'].' supprimée !"</H4>';
		outJS('top.frame_navigation.location.reload();', true);
	}
	echo "</form>";
}
// paramétrages divers
echo "<hr/>";
echo "<h2>".trad('LT_param').'</h2><form name="fparams" id="fparams"><p>';
dispInpHid("fparams", true, array("dir_echo"=>true));
echCheckBox("NoConfSuppr", 1, true, $_SESSION['parenv']["NoConfSuppr"] ? true : false);
echo trad('LT_noconfirmdelete')."<br/>";
dispInpTxt("nbligpp", $_SESSION['parenv']["nbligpp"] ? $_SESSION['parenv']["nbligpp"] : nbligpp_def, array("size"=>2, "dir_echo"=>true));
echo trad('LT_nblig_aff_ppage').'<br/>';
echCheckBox("noinfos", 1, true, $_SESSION['parenv']["noinfos"] ? true : false);
echo trad('LT_noinfos').'<br/>';
echCheckBox("ro", 1, true, $_SESSION['parenv']["ro"] ? true : false);
echo trad('LT_ro').'<br/>';
echo '<input type="submit" value="Valider"></p></form>';
echo '<hr>';
if (!$_SESSION['parenv']['admadm']) {
	echo '<form name="admadmident" id="admadmident" ><label for="admpasswd">Mot de passe administration</label> <input type="password" name="admadmpwd" size="15"> <input type="submit" value="valider"></form>';
} else {
	echo '<p>Vous êtes connecté en mode administration <a href="main.php?admadmpwd=deconn">deconnexion</a></p>';
	echo '<p><a href="main.php?cleanCache=true" class="fxbutton">Vider le cache disque des objets pyaobj de cette base</a></p>';	
	if ($_REQUEST['cleanCache']) {
		cleanPyaObjCache(($_SESSION['parenv']['bdd_name'] ? $_SESSION['parenv']['bdd_name'] : "*"), "*", "*");
	}
	echo '<p><a href="CREATE_DESC_TABLES.php" class="fxbutton">Assistant de configuration/information de bases de données</a></p>';
	dispLastAdmiSql();
}
 
echo "</div>"; // fin contenu_nav
echHtmlFooter('content');
