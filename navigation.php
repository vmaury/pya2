<?php 
include_once ('includes/config.inc.php');
// session_start_wthspid();
echHtmlHeader(true, 'frame_content');
//checkSessDBC();

if ($_REQUEST['db_name']) {
	$_SESSION['parenv']['bdd_name'] = $_REQUEST['db_name'];
	outJS('top.frame_content.location.href= "main.php?SESSION_NAME='.$_REQUEST['SESSION_NAME'].'"', true);
}
echo '<div style="clear:both"><img src="media/lim_knife_log.png" width="120" align="center" style="float:left; margin-right:10px;">';
echo '<small><u>Infos</u><br/>Serveur : '.$_SESSION['parenv']['bdd_host'];
echo ', type '.$GLOBALS['db_type']. ($GLOBALS['db_type'] == "mysql" ? ' ('.$GLOBALS['mysqlDrv'].')' : ''). '<br/>';
echo 'BDD : '. $_SESSION['parenv']['bdd_name'].'<br/>';
echo 'Utilisateur : '. $_SESSION['parenv']['user_id'].'<br/>';
echo '<a href="index.php?deconn=true" target="_top" title="forceOldMysqlDrv='.forceOldMysqlDrv.'">Déconnexion</a><br/>';
echo "</small></div>";
echo '<div id="contenu_nav" style="clear:both">';

$tbdbok = array();
if ($GLOBALS['db_type'] != "oracle") {
	$tblistdbs = db_show_bases();
	// liste toutes les bases; si diff oracle
	foreach ($tblistdbs as $dbname) {
		if (forceOldMysqlDrv) $GLOBALS['forceOldMysqlDrv'] = true;
		try {
			$GLOBALS['db_lnkid'] = db_connect($_SESSION['parenv']['bdd_host'], $_SESSION['parenv']['user_id'], $_SESSION['parenv']['user_pwd'], $dbname, $_SESSION['parenv']['bdd_pip']);
			$dbg = db_show_tables($GLOBALS["CisChpp"].$dbname.$GLOBALS["CisChpp"]);
			$admok = ($dbg && in_array($TBDname,$dbg));
		} catch (Exception $e) {
			$admok = false;
		}
		// n'affiche le lien pour edition que si la table d'admin existe dans la base
		if ($admok) {
			$tbdbok[$dbname] = ($dbname == $_SESSION['parenv']['bdd_name'] ? VSLD : "").$dbname.' ('.count($dbg).')';
			$bddok = $dbname; // au cas ou yen aie qu'une
		}
	}
} else { // oracle
	if (!$_SESSION['parenv']['admok']) { // on fait ça car c'est vachement long
		$dbg = db_show_tables($GLOBALS["CisChpp"].$_SESSION['parenv']['bdd_name'].$GLOBALS["CisChpp"]);
	  	$admok = ($dbg && in_array($TBDname,$dbg));
	  	$_SESSION['parenv']['admok'] = $admok ? "yes" : "no";
	} else {
		$admok = $_SESSION['parenv']['admok'] == "yes";
	}
  	if ($admok) {
		$bddok = $_SESSION['parenv']['bdd_name'];
		$tbdbok[] = $bddok;
	}
	// n'affiche le lien pour edition que si la table d'admin existe dans la base
}

if (count($tbdbok) == 1) {
	echo "<h4>Base  $bddok ".(is_array($dbg) ? " (".count($dbg).")" : "")."</h4>";
	$_SESSION['parenv']['bdd_name'] = $bddok;
} elseif (count($tbdbok) > 1) {
	$tbdbok = array(0=>'-') + $tbdbok;
	echo '<form name="fselbase" target="_self">';
	echo '<label for="db_name">'.trad('NAV_choixbdd').'</label><br/>';
	echo dispLD2($tbdbok, "db_name", array('force_type'=>'LDF', 'tbclesel' => array( $_SESSION['parenv']['bdd_name']), 'oth_att'=>' onChange="document.fselbase.submit()"')).'<br/>';
	//echo '<input type="submit" value="choisir">';
	echo '</form>';
}  else {
	echo "<h3>".trad('NAV_nobdded')."</H3>";
}
if ($_SESSION['parenv']['bdd_name']) { // une base est sélectionnée
	echo "<h3>".'<a href="main.php" target="frame_content"><img src="media/b_home.png"></a> '.$_SESSION['parenv']['bdd_name']."</h3>";
	if (forceOldMysqlDrv) $GLOBALS['forceOldMysqlDrv'] = true;
	$GLOBALS['db_lnkid'] = db_connect($_SESSION['parenv']['bdd_host'], $_SESSION['parenv']['user_id'], $_SESSION['parenv']['user_pwd'], $_SESSION['parenv']['bdd_name'], $_SESSION['parenv']['bdd_pip']);
	$dbg = db_show_tables($GLOBALS["CisChpp"].$_SESSION['parenv']['bdd_name'].$GLOBALS["CisChpp"]);
	$admok2 = ($dbg && in_array($TBDname,$dbg));
	// n'affiche le lien pour edition que si la table d'admin existe dans la base
	if ($admok2) {
		// affiche liste des tables fonction de ce qu'il y a dans TABLE0COMM
		$TYPAFFLHID=( $_SESSION['parenv']['admadm'] ? "" :  " AND (TYPAFF_L!='' OR  TYPAFF_L IS NOT NULL) AND NM_TABLE NOT LIKE '{$GLOBALS["id_vtb"]}%'"); // Chez Oracle - et effectivement contrairement à la norme - la chaîne vide '' est équivalenté à NULL.
		$rq = "SELECT NM_TABLE, LIBELLE, ".$GLOBALS["NmChpComment"]." as PYA_COMMENT from $TBDname where NM_CHAMP='$NmChDT' AND NM_TABLE != '$TBDname' AND NM_TABLE NOT LIKE '__reqcust' $TYPAFFLHID order by ORDAFF_L, LIBELLE";
		$tbltables = db_qr_comprass($rq);
		//echo $rq; print_r($tbltables);
		
		if (count($tbltables) > 0 && is_array($tbltables)) {
			if (isset($_SESSION['parenv']['tableFilter']) && !empty($_SESSION['parenv']['tableFilter'])) {
				$tbTableFilter = explode(',', $_SESSION['parenv']['tableFilter']);
			} else $tbTableFilter = false;
			//print_r($tbTableFilter);
			echo "<ul>\n";
			foreach ($tbltables as $tbl) {
				if ($tbTableFilter) {
					$booDispTable = false;
					foreach ($tbTableFilter as $filtTbl) {
						if (stristr($tbl['NM_TABLE'], $filtTbl)) {
							$booDispTable = true;
							break;
						}
					}
				} else $booDispTable = true;
				if ($booDispTable) {
					$lnk_edit_table = $_SESSION['parenv']['admadm'] ? '<a href="admdesct.php?lc_NM_TABLE='.$tbl['NM_TABLE'].'" title="modifier les propriétés PYA de cette table..."><img src="media/b_edit_table_small.png"/></a> ' : '';
					if (!$_SESSION['parenv']['ro']) $lnk_new = '<a href="edit_table.php?lc_NM_TABLE='.$tbl['NM_TABLE'].'" title="'.trad('LT_addrecord').'"> <img src="media/new_r.gif"></a> ';
					echo '<li'.(trim($tbl['PYA_COMMENT']) != "" ? ' title="'.$tbl['PYA_COMMENT'].'"' : '').'>'.$lnk_edit_table;
					if (!stristr($tbl['NM_TABLE'], $GLOBALS["id_vtb"])) {
						echo $lnk_new.'<a href="req_table.php?lc_NM_TABLE='.$tbl['NM_TABLE'].'">'.$tbl['LIBELLE'].'</a> <small>('.$tbl['NM_TABLE'].")</small>";
					} else echo $tbl['LIBELLE'].'<small>('.$tbl['NM_TABLE'].")</small>";
					echo "</li>\n";
				}
			}
			echo "<ul>\n";
		}
	}
	//print_r($tbltables);
}
echo "</div>"; // fin contenu_nav
echHtmlFooter('nav');