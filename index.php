<?php
include_once ('includes/config.inc.php');
if ($_REQUEST['deconn']) {
	unset($_SESSION);
	$_REQUEST['msg'] = "Vous vous êtes déconnecté";
}
if ($_REQUEST['authsm'] == 'true') { // formulaire d'auth soumis
	$_SESSION = array();
	$GLOBALS['db_type'] =  $_REQUEST['parenv']['db_type']; // $_REQUEST['parenv']['']
	if (forceOldMysqlDrv) $GLOBALS['forceOldMysqlDrv'] = true;
	//db_connect($_REQUEST['parenv']['bdd_host'], $_REQUEST['parenv']['user_id'], $_REQUEST['parenv']['user_pwd'], $_REQUEST['parenv']['bdd_name'], $_REQUEST['parenv']['IND_bddpip']);
	try {
		$link = db_connect($_REQUEST['parenv']['bdd_host'], $_REQUEST['parenv']['user_id'], $_REQUEST['parenv']['user_pwd'], $_REQUEST['parenv']['bdd_name'], $_REQUEST['parenv']['bdd_pip']);
		$_SESSION['parenv'] = $_REQUEST['parenv'];
		//setcookie("pyaSqlServ", $_REQUEST['parenv']['bdd_host'], time()+3600*24*60); // mémorise serveur pendant 2 mois
//		print_r ($link);
//		print_r($_SESSION);
	} catch (Exception $e) {
		$_REQUEST['msg'] = trad('IND_err_conn'); 	
	}
} else checkSessDBC(); // permet de court-circuiter la page d'auth dans le cas où on passe la chaine de connexion via le tableau $_REQUEST['tbRqParenv']

if ($_SESSION['parenv']['user_id']) {
	if (!$_REQUEST['parenv']['pyauser_id']) $_SESSION['parenv']['pyauser_id'] = $_SESSION['parenv']['user_id'];
	echHtmlHeader(false); 
	if ($_REQUEST['cleancache']) cleanPyaObjCache(); // vide tout le cache disque du serveur
	?>
	<frameset cols="300,*" rows="*" id="mainFrameset">
        <frame frameborder="0" id="frame_navigation" src="navigation.php" name="frame_navigation" />
        <frame frameborder="0" id="frame_content" src="main.php" name="frame_content" />
        <noframes>
	        <body>
	            <p>L'utilisation de phpYourAdmin n'est possible qu'avec un navigateur <b>supportant les "frames"</b>.</p>
	        </body>
	    </noframes>
	</frameset>
	</html>
<?php 
} else {
	echHtmlHeader(true); ?>
	<img src='media/lim_knife_log.png' width="300" align="center">
	<H1><?php echo trad("IND_title")?></H1>
	<?php if ($_REQUEST['msg']) echo "<H3>".$_REQUEST['msg']."</H3>";?>
	<?php echo trad("IND_txtacc")?>
	<form method="post" action="index.php" name="login_form" autocomplete="off" target="_top" class="login">
	    <fieldset><legend>Informations obligatoires</legend>
	   	<?php echo echParamIndex('IND_bddtype','parenv[db_type]',dispLD2($tb_dbtype, 'parenv[db_type]'))?>
	   	<?php echo echParamIndex('IND_choilang','parenv[CodLang]',dispLD2($tb_langs, 'parenv[CodLang]'))?>
	    <?php echo echParamIndex('IND_bdduser', 'user_id', '<input type="text" name="parenv[user_id]" id="user_id" value="" size="24" class="textfield"/>')?>
	    <?php echo echParamIndex('IND_bddpasswd', 'user_pwd', '<input type="password" name="parenv[user_pwd]" id="user_pwd" value="" size="24" class="textfield"/>')?>
	    <?php echo echParamIndex('IND_encoding','parenv[encod_type]',dispLD2($tb_encodes, 'parenv[encod_type]'))?>
	    </fieldset><fieldset><legend>Informations optionnelles</legend>
			<?php
			$inphost = "";
			if (file_exists("list_hosts.php")) {
				include ("list_hosts.php");
				if (is_array($tbhosts) && count($tbhosts) > 0) {
					$inphost = DispLDandTxt($tbhosts, "parenv[bdd_host]", 'localhost', false, "bdd_host");
				}
			}
			if ($inphost == "") $inphost = '<input type="text" name="parenv[bdd_host]" id="bdd_host" value="'.(isset($_COOKIE['pyaSqlServ']) ? $_COOKIE['pyaSqlServ'] : 'localhost').'" size="24" class="textfield"/>';
			?>
	   	<?php echo echParamIndex('IND_bddhost', 'bdd_host', $inphost)?>
	    <?php echo echParamIndex('IND_bddpip','bdd_pip','<input type="text" name="parenv[bdd_pip]" id="bdd_pip" value="" size="10" class="textfield"/>')?>
	    <?php echo echParamIndex('IND_bddname','bdd_name','<input type="text" name="parenv[bdd_name]" id="bdd_name" value="" size="24" class="textfield"/>')?>
	    <?php echo echParamIndex('IND_pyauser', 'pyauser_id', '<input type="text" name="parenv[pyauser_id]" id="pyauser_id" value="" size="24" class="textfield"/>')?>
	    <?php echo echParamIndex('IND_cleancache', 'cleancache', '<input type="checkbox" name="cleancache" id="cleancache" value="1" checked="checked"/>')?>
        </fieldset>
        <input value="true" type="hidden" name="authsm"/>
    <fieldset class="tblFooters">
        <input value="Exécuter" type="submit" id="input_go" />
    </fieldset>
</form>
	<?php 
	echHtmlFooter();
}
//
//echo $_REQUEST['SESSION_NAME'];
//echo '<a href="./">test</a>';

/**
 * 
 * Crache une ligne de paramètres à entrer
 * @param unknown_type $tradCode
 * @param unknown_type $htname
 * @param unknown_type $input
 */
function echParamIndex($tradCode,$htname,$input) {
	return '
        <div class="item">
            <label for="'.$htname.'">'.trad($tradCode).'</label>
            '.$input.'
        </div>
	';
}
