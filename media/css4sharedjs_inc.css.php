<?php 
ini_set("display_errors", "none");
$incpth = ini_get('include_path');
$cpath = (!stristr($incpth, "php_inc") ? "php_inc/" : "");
if (!file_exists($cpath.'fonctions.php')) $cpath = __DIR__."/../vendor/vmaury/php_inc/"; // en cas d'installation indépendante
if (!file_exists($cpath.'fonctions.php')) $cpath = __DIR__."/../../php_inc/"; // en cas d'installation en tant que package d'une autre app, du coup les 2 sont au même niveau

// fichier css4sharedjs_inc.css.php
header("Content-type: text/css; charset=utf-8");
// fichier partagé placé dans php_inc
// inclusions de fichiers partagés placés dans php_inc
// styles calendrier jQuery appelé sur input date 
include ($cpath."jQuery/css4sharedjs_inc.css.php"); 