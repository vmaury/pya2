<?php
ini_set("display_errors", "none");
$incpth = ini_get('include_path');
$cpath = (!stristr($incpth, "php_inc") ? "php_inc/" : "");
if (!file_exists($cpath.'fonctions.php')) $cpath = __DIR__."/../vendor/vmaury/php_inc/"; // en cas d'installation indépendante
if (!file_exists($cpath.'fonctions.php')) $cpath = __DIR__."/../../php_inc/"; // en cas d'installation en tant que package d'une autre app, du coup les 2 sont au même niveau

// fichier shared_inc.js.php
header("Content-type: text/javascript; charset=utf-8");
// inclusions de fichiers partagés placés dans php_inc
// fctions principales de jQuery
// calendrier sur input date
// francisation du calendrier
// init diverses JS dlcube
include ($cpath."jQuery/shared_inc.js.php");
