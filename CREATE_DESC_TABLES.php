<?php
// utilitaire permettant de générer la table DESC_TABLE qui décrit les autres

include_once ('includes/config.inc.php');
// session_start_wthspid();
checkSessDBC();
echHtmlHeader(true);
?>
<H1>Super Administration de phpYourAdmin</H1>
<H2>Assistant de création/MAJ de la table de description <?php echo $TBDname ?></H2> 
<?php
if ($_REQUEST['STEP'] == 0) {
	/*	 * **********************************************************************
	 *  ETAPE 1 **
	 * ********************************************************************* */
	?>
	<h2>Bonjour !</h2>
	Cette page va vous permettre de visualiser ou (re)générer la table utilitaire (nommée <b><?php echo $TBDname; ?></b>) permettant la description pour édition des autres tables de la base <b><?php echo $_SESSION['parenv']['bdd_name']; ?></b><br><br>
	<h3><u>Attention:</u> Les opérations qui suivent peuvent être excessivement risquées...</b></h3>
	<form method="post">
		<?php if ($GLOBALS['db_type'] != "oracle") { ?>
			Selectionner une base dans la liste et cliquez sur le bouton SUITE ci-dessous pour commencer ...<Br>
			<h3>Liste des bases du serveur <I><?php echo $_SESSION['parenv']['bdd_host'] ?></I></h3>

			<?php
			$tblbas = db_show_bases();
			foreach ($tblbas as $bas) {
				$tbldbas[$bas] = ($bas == $_SESSION['parenv']['bdd_name'] ? "#SEL#" : "") . $bas;
			}
			DispLD($tbldbas, "DBName");
			?>
			<br><br>
		<?php } ?>
		<input type="hidden" name="STEP" value="1">
		<input type="submit"  class="fxbutton" value="SUITE >>">
	</form><br>
	<?php
	//FIN SI STEP="", ie page d'accueil
} elseif ($_REQUEST['STEP'] == "1") {
	/***********************************************************************
	 *  ETAPE 2 **
	 ***********************************************************************/
	print_r($_REQUEST);
	if ($_REQUEST['DBName']) $_SESSION['parenv']['bdd_name'] = $_REQUEST['DBName'];
	?>
	<h2>Etape 2</h2>
	La base selectionnee est <B><U><?php echo $_SESSION['parenv']['bdd_name'] ?></B></U><br><br>
	Cette page va vous permettre de visualiser ou (re)geneer la table utilitaire (nommee <b><?php echo $TBDname; ?></b>) permettant la description pour edition des autres tables de la base <b><?php echo $_SESSION['parenv']['bdd_name']; ?></b><br><br>
	Vous pouvez editer un <a class="alertered14px" href="CONSULT_DESC_TABLES.php?DBName=<?php echo $_SESSION['parenv']['bdd_name'] ?>">état complet</a> ou un <a class="alertered14px" href="CONSULT_DESC_TABLES.php?DBName=<?php echo $_SESSION['parenv']['bdd_name'] ?>&simpl=1">etat simplifie</a> de la base</A><br><br>
	<h3><u>Attention:</u> Les opérations qui suivent peuvent être excessivement risquées...</b></h3>
	Selectionner une ou plusieurs tables (Ctrl+clic)dans la liste, et cliquez sur le bouton SUITE ci-dessous pour continuer ...<Br>
	<form name="theform" method="post">
		<table>
			<tr><td><h3>Liste des tables </h3>
					<?php
						$tbltab = db_show_tables($_SESSION['parenv']['bdd_name']);
						//print_r($tbltab);
					?>
					<select name="TableName[]" id="tablesList" multiple size="10">
						<?php
						if (in_array($TBDname, $tbltab)) {
							$tbdbdjp = db_qr_comprass("select distinct NM_TABLE from $TBDname");
							if ($tbdbdjp) {
								foreach ($tbdbdjp as $ltb) {
									$tbdbdjp2[] = $ltb['NM_TABLE'];
								}
							}
						}
						foreach ($tbltab as $rst) {
							$LNmTb .= $rst . ";"; // construit une chaine avec ts les noms de tables de la base
							if (strtolower($rst) != strtolower($TBDname))
								echo '<OPTION VALUE="' . $rst . '" ' . (in_array($rst, $tbdbdjp2) ? 'selected="selected"' : '') . '>' . $rst . '</OPTION>' . "\n";
						}
						?>
					</select>
					<br><a href="#" onclick="setSelectOptions('#tablesList', true); return false;">Tout selectionner</a>
					<br><a href="#" onclick="setSelectOptions('#tablesList', false); return false;">Tout deselectionner</a>
				</td>
				<td>
					<br>
					<input type="radio" name="CREATION" value="false">Consulter la table<br>
					<input type="radio" name="CREATION" value="check" checked>Vérifier la table, ie indiquer les différences entre les champs et tables présents dans <?php echo $TBDname; ?> et ceux effectivement présents dans la base <?php echo $_SESSION['parenv']['bdd_name']; ?><br>
					<input type="radio" name="CREATION" value="MAJ">Mettre a jour la table: les nouveaux champs crees, les anciens supprimes, mais les existants inchanges<br>
					<input type="radio" name="CREATION" value="vrai" onclick="if (this.checked) {
					alert('Soyez certain de vouloir re-geneer tout ou partie de la table de description !\n Toutes les valeurs prealablement saisies seront ecrasees si elles existent !');
				}" >(re)generer la table (!)<br/>
					<input type="radio" name="CREATION" value="AffCaract">Afficher seulement les caractéristiques des tables et champs de la Bdd Physique<br>
					<input type="radio" name="CREATION" value="Echantillon">afficher <input type="text" name="nbEchantillons" value="10" size="2"> échantillons des tables sélectionnées<br>
					<BR></h3>
					<input type="checkbox" name="AFFALL" value="vrai">Affichage des caracteristiques <u>completes</u> de chaque champ dans l'ecran suivant<BR><BR>
					<input type="checkbox" name="VALAUTO" value="vrai" checked="checked"> Affectation de valeurs automatiques pour certains champs en fonction de leur nom:<BR>
					&#149; champs contenant <input type="text" value="<?php echo DTMAJ ?>" name="vardtmaj" title="constante DTMAJ définie dans config.inc.php"> : date du jour auto<BR>
					&#149; champs contenant <input type="text" value="<?php echo DTCREA ?>" name="vardtcrea" title="constante DTCREA définie dans config.inc.php"> : date du jour auto si pas nulle avant<BR>
					&#149; champs contenant <input type="text" value="<?php echo USMAJ ?>" name="varusmaj" title="constante USMAJ définie dans config.inc.php"> : code user affecte à la MAJ, 
					<input type="checkbox" name="usmajlnkOK" value="vrai"> lié statique par la chaine <INPUT type="text" name="usmajlnk" value="<?php echo STRLIAISPERS ?>" title="constante STRLIAISPERS définie dans config.inc.php"><BR>
					&#149; champs contenant <input type="text" value="<?php echo USCREA ?>" name="varuscrea" title="constante USCREA définie dans config.inc.php"> : code user affecte, 
					<input type="checkbox" name="uscrealnkOK" value="vrai"> lié statique par la chaine <INPUT type="text" name="uscrealnk" value="<?php echo STRLIAISPERS ?>" title="constante STRLIAISPERS définie dans config.inc.php"><BR>

					<input type="hidden" name="DBName" value="<?php echo $_SESSION['parenv']['bdd_name'] ?>">
					<input type="hidden" name="STEP" value="2">
				</td></tr></table>
		<input type="submit" value="SUITE >>"  class="fxbutton">
	</form>
	<?php
} elseif ($_REQUEST['STEP'] == "2" || $_REQUEST['STEP'] == "CFADT2") {
	/*	 * **********************************************************************
	 *  ETAPE 3 **
	 * ********************************************************************* */
	/// AFFICHAGE OU MODIF DE LA TABLE
	unset($_SESSION['parenv']['admok']);
	if ($_REQUEST['STEP'] == "CFADT2") {
		$_REQUEST['CREATION'] = $_SESSION['CREATION'];
		$_REQUEST['TableName'] = $_REQUEST['TableName'];
	} else {
		$_SESSION['CREATION'] = $_REQUEST['CREATION'];
		$_SESSION['TableName'] = $_REQUEST['TableName'];
	}
	if ($_REQUEST['CREATION'] == "check") {
		echo "<H2>CHECK de la base " . $_SESSION['parenv']['bdd_name'] . " et de sa table de description $TBDname</H2>";
		$tbltab = db_show_tables($_SESSION['parenv']['bdd_name']);
		$trouve = in_array($TBDname, $tbltab); // table de description trouvée

		if (!$trouve) {
			echo "<H3>table de description $TBDname non trouvée dans la bdd " . $_SESSION['parenv']['bdd_name'] . ", veuillez la géréner</H3>";
		} else {
			echo "<H3> Test BDD -> $TBDname</H3>";
			foreach ($tbltab as $rt) {
				if ($rt != $TBDname) { // pas de test de la table de description
					$err = false;
					echo "<H4>Table $rt </H4>";
					$TbFieldList = db_qr_comprass("SHOW FIELDS FROM " . $rt);
					foreach ($TbFieldList as $InfoField) {
						if (!db_qr_comprass("select * from $TBDname where NM_TABLE='$rt' AND NM_CHAMP ='" . $InfoField['Field'] . "'")) {
							echo "Champ " . $InfoField['Field'] . " (" . $InfoField['Type'] . "," . $InfoField['Null'] . "," . $InfoField['Key'] . "," . $InfoField['Default'] . "," . $InfoField['Extra'] . ") absent de $TBDname<br/>";
							$err = true;
						}
						$tbTbChpBdd[$rt][$InfoField['Field']] = 1; // pr la suite
					} // fin boucle sur champs
					if (!$err)
						echo "==>OK<br/>";
				} // fin si pas table de desc
			} // fin boucle sur tables
			echo "<H3> Test $TBDname -> BDD</H3>";
			$tbd = db_qr_comprass("select * from $TBDname where NM_TABLE NOT LIKE '{$GLOBALS["id_vtb"]}%' AND NM_CHAMP != 'TABLE0COMM' ORDER BY NM_TABLE,NM_CHAMP");

			foreach ($tbd as $InfoField) {
				if ($InfoField['NM_TABLE'] != $tabprec) {
					if ($tabprec != "" && !$err)
						echo "==>OK<br/>";
					$tabprec = $InfoField['NM_TABLE'];
					$err = false;
					echo "<H4>Table $tabprec </H4>";
				}
				if ($tbTbChpBdd[$tabprec][$InfoField['NM_CHAMP']] != 1) {
					echo "Champ " . $InfoField['NM_CHAMP'] . " (Libelle:" . $InfoField['LIBELLE'] . ", typeAff:" . $InfoField['TYPEAFF'] . ", Valeurs" . $InfoField['VALEURS'] . ") absent de la BDD<br/>";
					$err = true;
				}
			}
		}
	} elseif ($_REQUEST['CREATION'] == "vrai" || $_REQUEST['CREATION'] == "MAJ") {
		echo "<H2>(RE)GENERATION DE LA TABLE $TBDname</H2>";
	} elseif ($_REQUEST['CREATION'] == "AffCaract") {
		echo "<H2>Consultation des caractéristiques de la BDD {$_SESSION['parenv']['bdd_name']}</H2>";
		echo '<table><THEAD><TH>NOM TABLE</TH><TH>NOM CHAMP</TH><TH>TYPE</TH><TH>LENGTH</TH></THEAD>';
	} elseif ($_REQUEST['CREATION'] == "Echantillon") {
		echo "<H2>Echantillons des table de la BDD {$_SESSION['parenv']['bdd_name']}</H2>";
	} else {
		echo "<H2>VISUALISATION DE LA BASE " . $_SESSION['parenv']['bdd_name'] . "</H2>";
	}

	if (count($_REQUEST['TableName']) > 0) {
		if ($_REQUEST['AFFALL'] == "vrai") {
			echo "<H3>Tables selectionnees : </H3><UL>";
			foreach ($_REQUEST['TableName'] as $Table) {
				echo"<LI> $Table";
			}
			echo "</UL>";
		}
		// test d'abord l'existence de la table DESC_TABLES
		$tbltab = db_show_tables($_SESSION['parenv']['bdd_name']);

		$trouve = in_array($TBDname, $tbltab); // table de description trouvée
		// effacement des enregistrements des tables qui n'existent plus
		// que si DESC_TABLES existe !
		if ($trouve && ($_REQUEST['CREATION'] == "vrai" || $_REQUEST['CREATION'] == "MAJ")) {
			$rqdt = db_query("select NM_TABLE from $TBDname group by NM_TABLE");
			while ($rpdt = db_fetch_row($rqdt)) {
				if (!in_array($rpdt[0], $tbltab) && !stristr($rpdt[0], $GLOBALS["id_vtb"]) && $rpdt[0] != "__reqcust") { // effacment si y est plus ou (table non virtuelle et pas requete custon
					db_query("delete from $TBDname where NM_TABLE='$rpdt[0]'");
					echo "<B>Enregistrements de la table <u>" . $rpdt[0] . " effacés!</u></b><BR>\n";
				}
			}
		}

		if ($trouve && ($_REQUEST['CREATION'] == "vrai") && count($_REQUEST['TableName']) > 0) {
			// effacement des enregistrements, mais uniquement ceux des tables s�ectionn�s
			foreach ($_REQUEST['TableName'] as $Table) {
				db_query("DELETE FROM $TBDname where NM_TABLE='$Table'") or die("Req. de vidage de  invalide !");
			}
		} elseif (!$trouve && $_REQUEST['CREATION'] != "vrai" && $_REQUEST['CREATION'] != "Echantillon") {
			echo "<BR><span class=\"normalred11px\">LA TABLE <B>$TBDname</B> n'existe pas ! Impossible de la visualiser</span><BR><br>";
			exit();
		} elseif ($_REQUEST['CREATION'] == "vrai") {
			// pour la requ�e, faire un copier coller de ce qui vient de phpmyadmin
			//print_r(db_show_tables($_SESSION['parenv']['bdd_name']));
			if (in_array($TBDname, db_show_tables($_SESSION['parenv']['bdd_name']))) {
				echo "Effacement de la table $TBDname existante...DROP TABLE $TBDname";
				db_query("DROP TABLE $TBDname");
			}
			$typchptxt = $GLOBALS["NmChpComment"] . ($GLOBALS['db_type'] != "oracle" ? " text" : " varchar2(2000)");
			$reqC = "CREATE TABLE $TBDname (
			 NM_TABLE varchar(50) NOT NULL,
			 NM_CHAMP varchar(50) NOT NULL,
			 LIBELLE varchar(250) NOT NULL,
			 ORDAFF_L varchar(5) DEFAULT '0' ,
			 TYPAFF_L varchar(10) DEFAULT 'AUT' ,
			 ORDAFF varchar(5) DEFAULT '0' ,
			 TYPEAFF varchar(20) DEFAULT 'AUT',
			 VALEURS " . ($GLOBALS['db_type'] != "oracle" ? " text" : " varchar2(2000)") . ",
			 VAL_DEFAUT varchar(200),
			 TT_AVMAJ varchar(255) ,
			 TT_PDTMAJ varchar(255),
			 TT_APRMAJ varchar(255),
			 TYP_CHP varchar(255),
			 $typchptxt,
			  PRIMARY KEY  (NM_TABLE,NM_CHAMP))";
			/* rab , pas vraiment indispensable, et qui ne fonctionne pas avec PostGresql
			  KEY NM_CHAMP (NM_CHAMP),
			  KEY NM_TABLE (NM_TABLE),
			  KEY ORDAFF_L (ORDAFF_L),
			  KEY ORDAFF (ORDAFF)) */
			db_query($reqC) or die("requete de creation invalide: <BR>$ReqC");
		} // creation et pas d'existence
		// debut remplissage de la table en fonction des autres

		foreach ($tbltab as $NM_TABLE) { // boucle sur les tables de la base
			$tbtoregen = false;
			foreach ($_REQUEST['TableName'] as $Table) { // boucle sur les tables sélectionnées dans la liste
				if ($Table == $NM_TABLE) {
					$tbtoregen = true;
					break;
				}
			}
			if ($_REQUEST['CREATION'] == "AffCaract" && $tbtoregen) {
				$resf = db_query(addwherefORlimit("select * from $CSpIC$NM_TABLE$CSpIC", 1)); // uniquement pour avoir la liste des champs
				$nfields = db_num_fields($resf);
				for ($j = 0; $j < $nfields; $j++)
					echo "<TR><TD>$NM_TABLE</TD><TD>" . db_field_name($resf, $j) . '</TD><TD>' . db_field_type($resf, $j) . '</TD><TD>' . db_field_size($resf, $j) . "</TD></TR>\n";
			} elseif ($_REQUEST['CREATION'] == "Echantillon" && $tbtoregen) {
				$resf = db_query(addwherefORlimit("select * from $CSpIC$NM_TABLE$CSpIC", (int)$_REQUEST['nbEchantillons']));
				$nfields = db_num_fields($resf);
				//echo "<H3><TABLE><TR><TD>Table</TD><TD>$NM_TABLE</TD></TR></TABLE></H3>\n";
				echo "<H3>$NM_TABLE</H3>\n";
				echo "<TABLE><THEAD><TR>\n";
				for ($j = 0; $j < $nfields; $j++)
					echo "<TD>" . db_field_type($resf, $j) . ":" . db_field_size($resf, $j) . "</TD>";
				echo "</TR><TR>\n";
				for ($j = 0; $j < $nfields; $j++)
					echo "<TH>" . db_field_name($resf, $j) . "</TH>";
				echo "</TR></THEAD>";
				while ($rw = db_fetch_assoc($resf)) {
					echo "<tr>";
					foreach ($rw as $v)
						echo "<td>$v</td>";
					echo "</tr>\n";
				}
				echo "</TABLE><BR/>\n";
			} else {
				if ($tbtoregen) { // table a regenérer ou à afficher
					$rqlibt = db_query("SELECT LIBELLE, " . $GLOBALS["NmChpComment"] . " from $TBDname where NM_TABLE='$NM_TABLE' AND NM_CHAMP='$NmChDT'");
					if (db_num_rows($rqlibt) > 0) {
						$rwlibt = db_fetch_assoc($rqlibt);
						$table0cexists = true;
					} else {
						$table0cexists = false;
						$rwlibt = array();
					}
					$ult = rtb_ultchp();
					echo '<a name="' . $NM_TABLE . '"> </a>';
					echo "<H3>Table <I>" . $NM_TABLE . "</I> (" . $rwlibt[$ult[LIBELLE]] . ")</H3>";

					if ($rwlibt[$ult[$GLOBALS["NmChpComment"]]])
						echo "<small>" . $rwlibt[$ult[$GLOBALS["NmChpComment"]]] . "</small><br/>";
					if ($_REQUEST['CREATION'] == "false") { // on affiche les champ dans l'ordre d�ition
						$resf = db_query("SELECT NM_CHAMP FROM $TBDname WHERE NM_TABLE='$NM_TABLE' AND NM_CHAMP!='$NmChDT' ORDER BY ORDAFF");
						while ($rf = db_fetch_row($resf))
							$tbLCHP[] = $rf[0];
					}
					$resf = db_query(addwherefORlimit("select * from $CSpIC$NM_TABLE$CSpIC", 1)); // uniquement pour avoir la liste des champs
					$nfields = db_num_fields($resf);

					if ($_REQUEST['AFFALL'] == "vrai")
						echo "<BLOCKQUOTE>La table $NM_TABLE comporte " . $nfields . " champs :<BR><FONT SIZE=\"-1\">";

					echo '<TABLE><THEAD><TH>NOM CHAMP</TH><TH>TYPE</TH><TH>LIBELLE</TH><TH>TYP. AFF</TH><TH>VALEURS</TH><TH>COMMENTAIRE</TH></THEAD>';

					// DU au fait que la fonction mysql_field_flags ne fonctionne correctement qu'avec un resultat "NORMAL" et pas avec une requete du type SHOW FIELDS
					if ($GLOBALS['db_type'] == "mysql")
						$table_def = db_query("SHOW FIELDS FROM $CSpIC$NM_TABLE$CSpIC");

					$rpct = db_query("SELECT NM_CHAMP FROM $TBDname WHERE NM_TABLE='$NM_TABLE' AND NM_CHAMP='$NmChDT'");
					if (db_num_rows($rpct) == 0)
						db_query("INSERT INTO $TBDname (NM_TABLE, NM_CHAMP,LIBELLE, ORDAFF, ORDAFF_L) values ('$NM_TABLE','$NmChDT','$NM_TABLE', '$i', '$i')");

					for ($j = 0; $j < $nfields; $j++) {
						echo "<TR><TD>";
						if ($GLOBALS['db_type'] == "mysql")
							$row_table_def = db_fetch_array($table_def);
						$NM_CHAMP = ($_REQUEST['CREATION'] != "false" ? db_field_name($resf, $j) : $tbLCHP[$j]);
						//$NM_CHAMP=$row_table_def['Field'];
						$tbNM_CHAMP[$j] = $NM_CHAMP;
						$TYP_CHAMP = "";
						$CREATMAJ = false;
						if ($_REQUEST['CREATION'] == "MAJ") {
							$rqCE = db_query("SELECT * from $TBDname where NM_TABLE='$NM_TABLE' AND NM_CHAMP='$NM_CHAMP'");
							// si champ pas existant il est a cr�r
							if (db_num_rows($rqCE) == 0)
								$CREATMAJ = true;
						}
						// cree l'enregistrement en MAJ ou 
						if ($_REQUEST['CREATION'] == "vrai" || $CREATMAJ) {
							// init sp�iales en fonction des noms ou des types de champs
							// des types, etc
							echo "<B><U>Creation </U></B>";
							$TT_AVMAJ = "";
							$TT_APRMAJ = "";
							$TT_PDTMAJ = "";
							$TYPEAFF = "AUT";
							$TYPAFF_L = "AUT";
							$COMMENT = "";
							$LIBELLE = $NM_CHAMP;
							$VALEURS = "";
							if ($_REQUEST['VALAUTO'] == "vrai") {
								if (stristr($NM_CHAMP, $_REQUEST['vardtmaj'])) {
									$TYPEAFF = "STA"; // affichage statique (non modifiable)
									$TYPAFF_L = ""; // pas d'affichage ds la liste 
									$TT_AVMAJ = "DJ"; // mise �jour auto de la date de MAJ
									$LIBELLE = "MAJ le";
								} elseif (stristr($NM_CHAMP, $_REQUEST['vardtcrea'])) {
									$TYPEAFF = "STA"; // affichage statique (non modifiable)
									$TYPAFF_L = ""; // pas d'affichage ds la liste 
									$TT_AVMAJ = "DJSN"; // mise �jour auto de la date de creation
									$LIBELLE = "Date Creation";
								} elseif (stristr($NM_CHAMP, $_REQUEST['varusmaj'])) {
									if ($_REQUEST['usmajlnkOK']) {
										$TYPEAFF = "STAL"; // affichage statique lié(non modifiable)
										$VALEURS = $_REQUEST['usmajlnk'];
									} else {
										$TYPEAFF = "STA"; // affichage statique 
									}
									$TYPAFF_L = ""; // pas d'affichage ds la liste
									$TT_AVMAJ = "US";
									$LIBELLE = "MAJ par";
								} elseif (stristr($NM_CHAMP, $_REQUEST['varuscrea'])) {
									if ($_REQUEST['uscrealnkOK']) {
										$TYPEAFF = "STAL"; // affichage statique li�(non modifiable)
										$VALEURS = $_REQUEST['uscrealnk'];
									} else
										$TYPEAFF = "STA"; // affichage statique 
									$TYPAFF_L = ""; // pas d'affichage ds la liste
									$TT_AVMAJ = "USSN";
									$LIBELLE = "Crée par";
								}
							} // fin si VALAUTO
							$val = $j; // force ordre d'aff sur 2 car
							if (strlen($val) == 1)
								$val = "0" . $val;
							if ($GLOBALS['db_type'] == "mysql") {
								if (stristr(db_field_type($resf, $j), "auto_increment")) {
									$TYPEAFF = "STA";
									$COMMENT = addslashes("Valeur auto incrémentée, impossible à changer par l'utilisateur");
								} // fin si champ auto incr�ent�	    
							} elseif ($GLOBALS['db_type'] == "pgsql") {
								if (strstr(db_field_type($resf, $j), "geometry")) {
									$TYPEAFF = "TXA";
									$TYPAFF_L = "";
									$TT_AVMAJ = "sql:astext(%1)";
									$TT_APRMAJ = "sql:geometryfromtext(%1)";
								}
							}

							db_query("INSERT INTO $TBDname 
				 (NM_TABLE, NM_CHAMP, LIBELLE, TYPEAFF, VALEURS, ORDAFF, ORDAFF_L, TYPAFF_L, TYP_CHP, TT_AVMAJ, TT_PDTMAJ,TT_APRMAJ," . $GLOBALS["NmChpComment"] . ")
				 values
				  ('$NM_TABLE', '$NM_CHAMP', '$LIBELLE', '$TYPEAFF', '$VALEURS', '$val', '$val','$TYPAFF_L', '$TYP_CHAMP', '$TT_AVMAJ','$TT_PDTMAJ','$TT_APRMAJ', '$COMMENT')");
						}  // fin si champ cr� dans la liste
						echo "<B>" . $NM_CHAMP . "</B> </TD>";
						if ($GLOBALS['db_type'] != "mysql")
							$row_table_def['Type'] = db_field_type($resf, $j);
						echo "<TD>" . $row_table_def['Type'];
						$row_table_def['True_Type'] = preg_replace('/\\(.*/i', '', $row_table_def['Type']);
						if ($_REQUEST['AFFALL'] == "vrai")
							echo " ;epuré " . $row_table_def['True_Type'] . "<BR>";
						if (strstr($row_table_def['True_Type'], 'enum')) {
							$enum = str_replace('enum(', '', $row_table_def['Type']);
							$enum = preg_replace('/\\)$/i', '', $enum);
							$enum = explode('\',\'', substr($enum, 1, -1));
							$enum_cnt = count($enum);
							if ($_REQUEST['AFFALL'] == "vrai") {
								echo "Liste de valeurs enum: ";
								for ($l = 0; $l < $enum_cnt; $l++) {
									echo $enum[$l] . " - ";
								}
								echo "<BR>";
							}
						} // fin si �um
						if ($_REQUEST['AFFALL'] == "vrai" && $GLOBALS['db_type'] == "mysql") {
							echo "db_field_type MySqli:" . db_field_type($resf, $j) . "<BR>";
						}
						echo "</TD>";

						$LIBELLE = RecupLib($TBDname, "NM_CHAMP", "LIBELLE", $NM_CHAMP);
						echo "<TD>" . ($LIBELLE != "" ? $LIBELLE : "&nbsp;") . "</TD>";
						$TYPEAFF = RecupLib($TBDname, "NM_CHAMP", "TYPEAFF", $NM_CHAMP);
						echo "<TD>" . $TYPEAFF . "</TD>";
						$VALEURS = RecupLib($TBDname, "NM_CHAMP", "VALEURS", $NM_CHAMP);
						echo "<TD>" . ($VALEURS != "" ? $VALEURS : "&nbsp;") . "</TD>";
						$COMMENT = RecupLib($TBDname, "NM_CHAMP", $GLOBALS["NmChpComment"], $NM_CHAMP);
						echo "<TD>" . ($COMMENT != "" ? $COMMENT : "&nbsp;") . "</TD>";

						echo "</TR>";
					} // fin boucle sur les champs de la table
					echo "</TABLE>";
					echo '<p><a href="admdesct.php?lc_NM_TABLE=' . $NM_TABLE . '&cfp=CDT' . '">Editer les propriétés de cette table</a></p>';
					// en MAJ on enl�e les champs plus existants
					if ($_REQUEST['CREATION'] == "MAJ") {
						$rqLCE = db_query("SELECT NM_CHAMP from $TBDname where NM_TABLE='$NM_TABLE' AND NM_CHAMP!='$NmChDT'");
						while ($rpLCE = db_fetch_row($rqLCE)) {
							// si champ n'existe plus l'enl�e
							if (!in_array($rpLCE[0], $tbNM_CHAMP)) {
								db_query("DELETE FROM $TBDname where NM_TABLE='$NM_TABLE' AND NM_CHAMP='$rpLCE[0]'");
								echo "<br>Champ <b>$rpLCE[0] <u>supprimé</u></b> de la table de description! <BR>";
							} // fin si a supprimer
						}
					} // fin si MAJ
					echo "</FONT></BLOCKQUOTE>";
				} // fin si pas table de definition des autres
				$i++;
			} // fin si pas caract
		} // fin boucle sur les tables de la bass
	} // si nbre tables selectionnn�s >0
	elseif ($_REQUEST['CREATION'] != "check")
		echo "<H3> Vous devez selectionner au moins une table !</H3>";
} // fin tests sur step
if ($_REQUEST['STEP'] > 0)
	echo '<br><a href="' . $_SERVER['SCRIPT_NAME'] . '?STEP=' . ($_REQUEST['STEP'] - 1) . '" class="fxbutton"> << RETOUR</A>';
?>
<script>
	/** selectionne tout ou rien d'une LD
	 * 
	 * @param {type} theId ou theClass
	 * @param {type} bool
	 * @returns {undefined}
	 */
	function setSelectOptions(theId, bool) {
		$(theId + ' option').prop('selected', bool);
	}
</script>
<?php
echHtmlFooter('nav');
