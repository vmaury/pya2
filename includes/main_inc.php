<?php
$incpth = ini_get('include_path');
$cpath = (!stristr($incpth, "php_inc") ? "php_inc/" : "");
if (!file_exists($cpath.'fonctions.php')) $cpath = __DIR__."/../vendor/vmaury/php_inc/"; // en cas d'installation indépendante
if (!file_exists($cpath.'fonctions.php')) $cpath = __DIR__."/../../php_inc/"; // en cas d'installation en tant que package d'une autre app, du coup les 2 sont au même niveau

//die ($incpth."#".$cpath."#");
//$cpath = "../php_inc/";
if (!include_once ($cpath.'fonctions.php')) die("Impossible d'inclure ".$cpath."fonctions.php");
define('path2phpInc',$cpath);
include ('spc_functions_4pya.php');
// démarre session avec id spécifique
ini_set('session.gc_maxlifetime', 1800); // session réduite à 1/2h 
ini_set('session.lifetime', 1800); // session réduite à 1/2h

session_start_wthspid(diffSess);
//$resSs =  session_start();
//echo "<!-- ".basename($_SERVER['SCRIPT_NAME']) ." = |$resSs| \n";
//print_r($_SESSION);
//echo '-->';

if (basename($_SERVER['SCRIPT_NAME']) != "index.php") checkSessDBC();

ini_set("default_charset", $_SESSION['parenv']['encod_type'] ? $_SESSION['parenv']['encod_type'] : "utf-8");
if (!isset($_SESSION['nbligpp'])) $_SESSION['nbligpp'] = nbligpp_def;
// gestion lang
if (!isset($_SESSION['parenv']['NoLang'])) { 
	// détection langue du navigateur
	$Langue = explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);
	$Langue = strtolower(substr(chop($Langue[0]),0,2)); // fr, en etc...
	if ($Langue != "fr") {
		$_SESSION['parenv']['NoLang'] = 1 ; // anglais si pas francais
		$_SESSION['parenv']['CodLang'] = 'en' ; // anglais si pas francais
	} else
		$_SESSION['parenv']['NoLang'] = 0 ; // francais par défaut
		$_SESSION['parenv']['CodLang'] = 'fr' ; 
}
include("lang_".$_SESSION['parenv']['CodLang'].".php");