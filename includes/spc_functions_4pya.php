<?php
/**
 * Fonctions spécifiques à PYA
 */

/**
 * Crache l'entete html
 */
function echHtmlHeader($dbody = true, $btarget = '_self', $custJS = '') {
	if ($custJS) {
		$arg4ijs = '?custJS='.urlencode($custJS);
	}
	
	header('Content-type: text/html; charset='.($_SESSION['parenv']['encod_type'] ? $_SESSION['parenv']['encod_type'] : "utf-8")); 
	
echo '
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr" dir="ltr">
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset='.($_SESSION['parenv']['encod_type'] ? $_SESSION['parenv']['encod_type'] : "utf-8").'" />
	    <title>phpYourAdmin </title>
	    <link rel="icon" href="./media/favicon.ico" type="image/x-icon" />
	    <link rel="shortcut icon" href="./media/favicon.ico" type="image/x-icon" />';

if ($dbody) echo '
	    <link rel="stylesheet" type="text/css" href="media/pya2.css" />
	    <link rel="stylesheet" type="text/css" href="media/css4sharedjs_inc.css.php" />
	    <script type="text/javascript" src="./media/shared_inc.js.php'.$arg4ijs.'"></script>
		<style type="text/css">@import url(./media/css4sharedjs_inc.css.php);</style>';

echo '<meta name="robots" content="noindex,nofollow" />
	    <base target="'.$btarget.'">
	</head>';
	if ($dbody) {
//		echo "<!--";
//		print_r($_SESSION);
//		echo "-->";
		echo "<body>\n";	
	}
}

/**
 * Crache le footer html
 */
function echHtmlFooter($type='') {
	if ($type != 'nav') {
		echo '<div id="footer">phpYourAdmin est un outil libre GPL développé par la <span title="'.(md5($_SERVER["SERVER_NAME"])).'">SSLL</span> Limousine <a href="www.dlcube.com" target="_blank">DLCube</a></div>';
	}
	echo "</body></html>\n";
}

/**
 * Check si session toujours valide, et (re)connecte à la BDD
 */
function checkSessDBC() {
	
	if ($_REQUEST['tbRqParenv']) { // permet d'appeler une page (frame)de pya2 directement 
		//$_SESSION['parenv'] = unserialize(urldecode($_REQUEST['tbRqParenv'])); // bah ça ça marche pas ... le urldecode fout la merde
		$_SESSION['parenv'] = unserialize($_REQUEST['tbRqParenv']);
  		//print_r($_SESSION['parenv']);
	} 
	if (defined("configLocExists") && configLocExists == 1) {
		if (defined('dbUserName')) {
			$_SESSION['parenv']['db_type'] = dbType;
			$_SESSION['parenv']['bdd_host'] = dbHost;
			if(defined('dbDbName')) {
				$_SESSION['parenv']['bdd_name']  =  dbDbName;
			} //else unset($_SESSION['parenv']['bdd_name']);
			$_SESSION['parenv']['user_id'] = dbUserName;
			$_SESSION['parenv']['user_pwd'] = dbUserPasswd;
			$_SESSION['parenv']['bdd_pip'] = dbPort;
			$_SESSION['parenv']['encod_type'] = dbEncType;
		}
	}
	if (empty($_SESSION['parenv']['user_id'])) { 
	// on teste celui-là, car on est sûr qu'il est indispensable
		if (basename($_SERVER['SCRIPT_NAME']) != "index.php") outJS("window.top.location.href = 'index.php?msg=".urlencode('Session expirée')."'", true);// attention aux bouclages
		//echo '<!--'.basename($_SERVER['SCRIPT_NAME']).' a paumé parenv-user_id-->';
	} else {
		$GLOBALS['db_type'] =  $_SESSION['parenv']['db_type']; // $_REQUEST['parenv']['']
		if (forceOldMysqlDrv) $GLOBALS['forceOldMysqlDrv'] = true;
		$link = db_connect($_SESSION['parenv']['bdd_host'], $_SESSION['parenv']['user_id'], $_SESSION['parenv']['user_pwd'], $_SESSION['parenv']['bdd_name'], $_SESSION['parenv']['bdd_pip'], str_replace("-","",$_SESSION['parenv']['encod_type']));
	}
	if (isset($_REQUEST['ro'])) $_SESSION['parenv']['ro'] = $_REQUEST['ro'];
} 

/**
 * fonction qui transforme les *clés* d'un tableau qui ne sont pas en majuscule en majuscule (sert pour PostGres)
 */
function case_kup($tb) {
	if (is_array($tb)) { foreach ($tb as $cle=>$val) {
		$ret[strtoupper($cle)]=$val;
	}}
	return($ret);
}

/**
 * affiche derniere sql admin
 */
function dispLastAdmiSql() {
	if (trim($_SESSION["LastAdmAction"]) != '') echo '<small><br/>Sql dernière action admin '. toggleAffDiv("LastAdmAction","<pre>".$_SESSION["LastAdmAction"]."</pre>","","Sql Dernière action admin",false).'</small>';
}