<?php 
include_once ('includes/config.inc.php');
// session_start_wthspid();
//checkSessDBC();

//$whodb=stripslashes(urldecode($whodb));
$whodb = stripslashes($_REQUEST['whodb']);
$ult = rtb_ultchp(); // tableau des noms de champs sensibles �la casse (�cause de pgsql...)

$debug=false;
//print_r($_SESSION);
// entetes http pour t��hargement
if (!$debug) {
header('Content-disposition: filename=extractPYA.tsv');
header('Content-type: application/octetstream');
header('Content-type: application/ms-excel');
header('Pragma: no-cache');
header('Expires: 0');
}
if ($_REQUEST['lc_NM_TABLE']) { // on vient de la page de req
	$NM_TABLE = $_REQUEST['lc_NM_TABLE'];
} else $NM_TABLE = $_SESSION['NM_TABLE'];

$tab="\t"; //tab en ascii
echo "Extraction de donnees de phpYourAdmin\n\n";
echo "Base ".$_SESSION['parenv']['bdd_name']."\n\n";
if ($debug) {
//	echovar("_SESSION");
//   echovar("_REQUEST");
	

   echovar("whodb");
   echovar("TBDname");
   echovar("NM_TABLE");
   echovar("tbAfC");
}

if ($NM_TABLE != "__reqcust") {
   // recup libelle et commentaire de la table
   $LB_TABLE = epurelongchp(RecLibTable($NM_TABLE,0));
   echo "Edition de la table $LB_TABLE ($NM_TABLE)\n\n";

   $result = db_query("SELECT 1 FROM ".$GLOBALS['CSpIC'].$NM_TABLE.$GLOBALS['CSpIC']." $whodb");
   // on compte le nombre de ligne renvoyee par la requ�e
   }
else { // req custom
   $result = db_query($_SESSION['reqcust']);
   $LB_TABLE = $ss_parenv['lbreqcust'];
   $COM_TABLE="";
}

if ($_REQUEST['encod']=="iso") {
	$LB_TABLE = utf8_decode($LB_TABLE);
}

$nbrows = db_num_rows($result);

if ($nbrows==0) {
  echo "AUCUN ENREGISTREMENT !\n" ;
} else {
  if ($NM_TABLE != "__reqcust") {
     $reqstd = "select * from ".$GLOBALS['CSpIC'].$NM_TABLE.$GLOBALS['CSpIC'];
     
     $rq1 = db_query("select * from $TBDname where NM_TABLE='$NM_TABLE' AND NM_CHAMP!='$NmChDT' AND TYPAFF_L!='' ORDER BY ORDAFF_L, LIBELLE");
     $nbcol = 0; // n de colonne
	 if (db_num_rows($rq1)) { // la table est décrite ...
		while ($res0 = db_fetch_assoc($rq1)) {
			$tbobjCC[$nbcol] = $res0[$ult['NM_CHAMP']];
			if ($_SESSION["tbAfC"][$res0[$ult['NM_CHAMP']]])  $nbcol++;// la condition n'est true que si champ à afficher et case cochée
			//$nbcol++;
		}
		$nbcol=($nbcol-1);
		for ($i=0; $i<=$nbcol; $i++){
          $NomChamp = $tbobjCC[$i];
          $CIL[$NomChamp] = createOrLoadPyaObj($_SESSION['parenv']['bdd_name'], $NM_TABLE, $NomChamp);
          $CIL[$NomChamp]->InitPO();
		} // fin boucle sur les champs
	 } else {
		 $CIL = InitPOReq($reqstd,$_SESSION['parenv']['bdd_name']); // construction ey initialisation du tableau d'objets
		 $nbcol = count($CIL) -2;
	 }
     
     
     
     } // fin si pas custom

  else { // requete custom (perd l'ordre d'affichage sinon)
       $CIL = InitPOReq($_SESSION['reqcust'],$_SESSION['parenv']['bdd_name']); // construction ey initialisation du tableau d'objets
  }

 // echovar("CIL");
// si   infos, affiche les vrais noms de champs
  if (!$_SESSION['parenv']['noinfos']) {
      echo "Noms des champs de la Bdd\t";
      foreach ($CIL as $objCIL){ // boucle sur le tableau d'objets colonnes
          if ($objCIL->Typaff_l!="" && $objCIL->Typaff_l!="hid") echo  strip_tags($objCIL->NmChamp).$tab;
        }
      echo "\n";
      echo "\n";
  } // fin si afichage des noms de champs  
  
  // affichage ent�es de lignes (noms des champs en clair)
  echo "N ligne\t";
  
  foreach ($CIL as $objCIL){ // boucle sur le tableau d'objets colonnes
     $objCIL->NbCarMxCust = 5000;
     $NomChamp = $objCIL->NmChamp;
     if ($objCIL->Typaff_l != "" && $objCIL->Typaff_l!="") {
     	echo strip_tags($_REQUEST['encod']=="iso" ? utf8_decode($objCIL->Libelle) : $objCIL->Libelle).$tab;
     }
     else unset($CIL[$NomChamp]); // en profite pour supprimer les champs non affiches
         
    }
  echo "\n";

  $req = db_query(($NM_TABLE != "__reqcust" ? $reqstd : $_SESSION['reqcust'])." $whodb");
  $i=1;
  while ($tbValChp =db_fetch_array($req)) {
    // colonnes 
      echo $i.$tab; // affiche No de ligne
      $i++;
      foreach ($CIL as $kc => $objCIL){ // boucle sur le tableau d'objets colonnes
		  if (is_object($objCIL)  && $kc != 'db_resreq') {
			$NomChamp = $objCIL->NmChamp;
			$objCIL->ValChp = strip_tags($tbValChp[$NomChamp]);
			if ($_REQUEST['encod']=="iso") {
				if ($objCIL->TTC=="numeric") $objCIL->ValChp = str_replace(".",",",$objCIL->ValChp);
				echo  utf8_decode(epurelongchp($objCIL->RetVCL(false),false)).$tab;
			} else  echo epurelongchp($objCIL->RetVCL(false),false).$tab; // affiche Valeur Champ ds Liste
		  } // fin si objet
      }  // fin boucle sur les colonnes
      echo "\n";
    } // fin while = fin boucle sur les lignes
  } // fin si nbrows>0

function epurelongchp($vchp, $tronq=true)  
{  
  $vchp=str_replace("\n", ";",$vchp);
  $vchp=str_replace("\r", ";",$vchp);
  $vchp=str_replace("&nbsp;", " ",$vchp);
  $vchp=str_replace("<br>", ";",$vchp);
  $vchp=str_replace("<BR>", ";",$vchp);
  if ($tronq) $vchp = substr($vchp,0,255) ;
  return($vchp);
}
?>

