<?php
include_once ('includes/config.inc.php');
// session_start_wthspid();
checkSessDBC();
if ($_REQUEST['lc_NM_TABLE']) {
	$_SESSION['NM_TABLE'] = $_REQUEST['lc_NM_TABLE'];
	unset($_SESSION['tbchptri']);
	unset($_SESSION['tbordtri']);
	$_SESSION['forceRRq'] = true; // pour forcer le calcul en liste après validation
}
$NM_TABLE = $_SESSION['NM_TABLE'];
if ($_REQUEST['modif'] == "C") {
	$modif = 1;
} else $modif = $_REQUEST['modif'];
if (!$_REQUEST['cfp'])  $_REQUEST['cfp'] = "list_table.php"; // page appelante, si pas spécifiée
if ($NM_TABLE != "__reqcust") {
	// recup libelle et commentaire de la table
	$LB_TABLE = RecLibTable($NM_TABLE,0);
	$COM_TABLE = RecLibTable($NM_TABLE,1);
	
	if ($modif==1)	{
		$lbtitre = ($_SESSION['parenv']['ro'] ? trad('com_consultation') : trad('com_edition')).trad('ER_record_of_table'). $LB_TABLE;
	} else if ($modif==2) {
		$lbtitre = trad('com_copy').trad('ER_record_of_table').$LB_TABLE;
	} else {
		$lbtitre = trad('com_add').trad('ER_record_of_table').$LB_TABLE;
	}
} else {
	$LB_TABLE = $ss_parenv[lbreqcust];
	$COM_TABLE = "";
	$lbtitre = trad('ER_record_details');
}

$title = trad('com_edition').trad('com_record')." $NM_TABLE (DB $DBName)";

$key = stripslashes(urldecode($_REQUEST['key']));
echHtmlHeader(true);
if ($debug) DispDebug();
?>
<div align="center">
<H1><?php echo$lbtitre; ?></H1>
<?php if (!$_SESSION['parenv']['noinfos']) { ?>
	<small><?php echo$COM_TABLE;?></small>
	<H6>
	<?php echo ($modif==1 ? trad('ER_record_car').$key : ""); ?>
	</H6>
	<?php echo ($modif==2 ? "<H5><u>COPIE!</u> pensez a changer la cle ($key) si elle n'est pas en auto-increment !<br>- Attention les images ou fichiers lies ne sont pas copies !</H5>" : ""); ?>
<?php } ?>
<BR>

<script language="Javascript">
function ConfReset() {
	if (confirm('<?php trad('ER_raz_confirm')?>')) document.theform.reset();
}
</script>
<form action="amact_table.php" method="post" name="theform" id="theform" ENCTYPE="multipart/form-data" onsubmit="return testJSValChp(this);">
<?php 
echochphid('key', $modif != 2 ? $key :"", true);
echochphid('modif', $modif, true);
echochphid('cfp', $_REQUEST['cfp'], true);
echochphid('SESSION_NAME', $_REQUEST['SESSION_NAME'], true);

if ($_REQUEST['errMaj']) echo "<H2>Erreur d'enregistrement : ".$_REQUEST['errMaj']."</H2>"; 

$reqLChp = "SELECT NM_CHAMP from $TBDname where NM_TABLE='$NM_TABLE' AND NM_CHAMP!='$NmChDT' ORDER BY ORDAFF, LIBELLE"; // on a besoin de TOUTES les valeurs à cause des clés multiples éventuelles

if (($modif==1 || $modif==2) && !$_REQUEST['errMaj']) { // recup des valeurs de l'enregistrement
	$where = "  where ".$key.($where_sup=="" ? "" : " and $where_sup");
	if ($NM_TABLE != "__reqcust") {
		//$reqcust="SELECT * FROM $CSpIC$NM_TABLE$CSpIC"; on ne fait plus ca, car on charge les champs blobs qui sont caches pour rien... 
		$rq1 = db_query($reqLChp);
		while ($rw = db_fetch_row($rq1)) {
			$tbc[] = $rw[0];
		}
		$lctd = implode(",",$tbc);
		$reqcust="SELECT $lctd FROM ".$GLOBALS['CSpIC'].$NM_TABLE.$GLOBALS['CSpIC'];
	}
	$req = db_query($reqcust." ".$where);
	$tbValChp = db_fetch_assoc($req);
}
if ($_REQUEST['errMaj']) $tbValChp = $_REQUEST;;

if ($NM_TABLE != "__reqcust") {
	// Creation et Initialisation des propriétés des objets PYAobj
	$rq1 = db_query($reqLChp);
	while ($CcChp = db_fetch_row($rq1)) { // boucles sur les champs
		$NM_CHAMP = $CcChp[0];
		$ECT[$NM_CHAMP] = createOrLoadPyaObj($_SESSION['parenv']['bdd_name'], $NM_TABLE, $NM_CHAMP);
		$ECT[$NM_CHAMP]->DirEcho = true;
		$ECT[$NM_CHAMP]->dialFormId = "theform"; // pr les ctrl Ajax
		//$ECT[$NM_CHAMP] = new PYAobj();
		//$ECT[$NM_CHAMP]->NmBase = $_SESSION['parenv']['bdd_name'];
		//$ECT[$NM_CHAMP]->NmTable = $NM_TABLE;
		//$ECT[$NM_CHAMP]->NmChamp = $NM_CHAMP;
		//$ECT[$NM_CHAMP]->InitPO();
		$ECT[$NM_CHAMP]->TypEdit = $modif;
		if (strstr($ECT[$NM_CHAMP]->TypeAff,"POPL")) $poplex=true; // s'il existe au moins une edition en popup li� fait tt le temps
	}
} else { // requete custom
	$ECT = InitPOReq($reqcust." ".$where,$DBName);
}
if ($poplex) JSpopup(); // s'il existe au moins une edition en popup lié colle le code d'ouverture d'une popup
?>
<TABLE class="edittable">
<?php
foreach ($ECT as $PYAObj) {
	if ($_SESSION['parenv']['ro'] || $NM_TABLE=="__reqcust" || $_REQUEST['modif'] =='C') $PYAObj->TypEdit = "C"; // en consultation seule en readonly ou req cust
	$NM_CHAMP = $PYAObj->NmChamp;
	if ($modif != "") $PYAObj->AffVal($tbValChp); // si pas creation (edit ou copy) recup la val

	if ($modif == 2) { // en cas de COPIE on annule la valeur auto incremente
		if (stristr($PYAObj->FieldExtra,"auto_increment")) $PYAObj->ValChp="";
	}
	// traitement valeurs avant MAJ
	$PYAObj->InitAvMaj($_SESSION['parenv']['pyauser_id']);
	if ($PYAObj->TypeAff!="HID" && $PYAObj->TypeAff!="") {
		$PYAObj->EchoCompField("Edit", array("modTable" => true, 'dispComment' => true));
//		echo "<TR><TD>".$PYAObj->Libelle;
//		if ($PYAObj->Comment!="") echo "<BR><span class=\"legendes9px\">".$PYAObj->Comment."</span>";
//		echo "</TD>\n<TD>";
//	 	$PYAObj->EchoEditAll(false); // n'affiche pas les champs cachés
//		echo "</TD>\n</TR>"; //finit la ligne du tableau
	} else	$PYAObj->EchoEditAll(true); // affiche champs cachés

} // fin boucle sur les champs
echo '</table>
';
echo '
<div ALIGN="center">
<br>
';

echo '<a href="'.$_REQUEST['cfp'].'" class="fxbutton">'.(!$_SESSION['parenv']['ro'] ? "Annuler tous les changement et ": "").'retour à la liste</A>';
// boutons valider et annuler que quand read only false
if (!$_SESSION['parenv']['ro'] && $_REQUEST['modif'] != 'C') {
	echo nbsp(4);
	echo '<a href="'.($poplex ? "closepop();" : "").'javascript:ConfReset()" class="fxbutton"> '.trad('BT_reset').' </a>';
	echo nbsp(4);
	echo '<input type="submit" class="fxbutton" value="'.trad('BT_valider').'" title="Valider les changements et retourner à la liste">';
	if ($_REQUEST['modif'] == 1) {
		echo nbsp(4);
		echo '<input type="submit" name="btmaj" class="fxbutton" value="Mise à jour"  title="Valider les changements et rester sur cette page">';
	}
}
echo '<br/><br/>';
?>
</div>
<?php echHtmlFooter('nav');
