<?php
include_once ('includes/config.inc.php');
// session_start_wthspid();
//checkSessDBC();
//print_r($_REQUEST);
$ult=rtb_ultchp(); // tableau des noms de champs sensibles a la casse (a cause de pgsql...)
if ($_SESSION['parenv']['nbligpp'] < 1) $_SESSION['parenv']['nbligpp'] = nbligpp_def;
if (isset($_REQUEST['lc_FirstEnr'])) {
	$_SESSION["FirstEnr"] = $_REQUEST['lc_FirstEnr'] ; 
}
if ($_REQUEST['lc_NM_TABLE']) { // on vient de la page de req
	$_SESSION['NM_TABLE'] = $NM_TABLE = $_REQUEST['lc_NM_TABLE'];
	unset($_SESSION['tbchptri']);
	unset($_SESSION['tbordtri']);
	$_SESSION["FirstEnr"] = 0; 
	$_SESSION['forceRRq'] = true; // pour forcer le calcul en liste
} else $NM_TABLE = $_SESSION['NM_TABLE'];

// gestion index 1er enregistrement (pagination)
if (!isset($_SESSION["FirstEnr"]) || $_REQUEST['cfopl'] != "" ) { // && cfopl=="" on vient d'une autre page que de celle la
	unset($_SESSION["tbchptri"]); 
	unset($_SESSION["tbordtri"]); 
	$_SESSION["FirstEnr"] = 0; 
	$orderb = "";
}  

// gestion clic sur tri sur champ  
if ($_REQUEST['chptri']!="") {
	// test si champ existant deja dans les tri secondaires
	if ($_SESSION['tbchptri'][2] == $_REQUEST['chptri']) {
		$_SESSION['tbchptri'][2] = $_SESSION['tbchptri'][3];
		$_SESSION['tbordtri'][2] = $_SESSION['tbordtri'][3];
	}
	if ($_SESSION['tbchptri'][3] == $_REQUEST['chptri']) {
		unset ($_SESSION['tbchptri'][3]);
		unset ($_SESSION['tbordtri'][3]);
	}
	  
	if ($_SESSION['tbchptri'][1]!=$_REQUEST['chptri']) {
		 // dcale les ordres de tri si le champ de tri est different  
		if ($_SESSION['tbchptri'][2] != "") {
			$_SESSION['tbchptri'][3] = $_SESSION['tbchptri'][2];
			$_SESSION['tbordtri'][3] = $_SESSION['tbordtri'][2];
		}
		if ($_SESSION['tbchptri'][1] != "") {
			$_SESSION['tbchptri'][2] = $_SESSION['tbchptri'][1];
			$_SESSION['tbordtri'][2] = $_SESSION['tbordtri'][1];
		}
	}
	$_SESSION['tbchptri'][1] = $_REQUEST['chptri'];
	$_SESSION['tbordtri'][1] = $_REQUEST['ordtri'];
	  
	$_SESSION["FirstEnr"] = 0;
} // fin $_REQUEST['chptri']!=""

if ($_SESSION['tbchptri'][1]!="") {
  $orderb = " ORDER BY ".$_SESSION['tbchptri'][1]." ".$_SESSION['tbordtri'][1];
  if ($_SESSION['tbchptri'][2] != "") {
  	$orderb .= ", ".$_SESSION['tbchptri'][2]." ".$_SESSION['tbordtri'][2];
  	if ($_SESSION['tbchptri'][3] != "") $orderb .= ", ".$_SESSION['tbchptri'][3]." ".$_SESSION['tbordtri'][3];
  }
  
}
// page appelante spécifiée
if (isset($_REQUEST['lc_PgReq']) || $_SESSION['forceRRq'] ) { //|| true// on vient d'une autre page que celle-la ou d'un nouvel enregistrement appelé par la nav donc il faut recalculer la req
	if (!empty($_SESSION['forceRRq'])) {
		unset($_SESSION['forceRRq']);
	} 
	if (!empty($_REQUEST['lc_PgReq']))	{
		$_SESSION["PgReq"] = $_REQUEST['lc_PgReq'];
	}
	// on est pas en requete custom
	if ($NM_TABLE != "__reqcust" && $_REQUEST['lc_reqcust'] == "") {
		// recup libelle et commentaire de la table
		$LB_TABLE = RecLibTable($NM_TABLE,0);
		$COM_TABLE = RecLibTable($NM_TABLE,1);

		// constitution du where et des colonnes a afficher en fonction des criteres de requetes eventuels
		$rqrq = db_query("select NM_CHAMP,TYPAFF_L from $TBDname where NM_TABLE='$NM_TABLE' AND NM_CHAMP!='$NmChDT'");
		// on balaye les noms de champs de cette table
		$condexists = false;
		$afcexists = false;
		
		unset ($_SESSION['memFilt']);
		
		$first = true;
		while ($rwrq = db_fetch_row($rqrq)) {
			// reconstitution nom de la var du Type Requete
			$cond = retCondPya($_SESSION['parenv']['bdd_name'], $NM_TABLE, $rwrq[0]);			
			if ($cond != "") {
				$condexists = true;
				if ($where_sup!="") $where_sup .= " AND ";
				$where_sup .= $cond;
			}
			// on teste maintenant l'existence de variables de colonnes �afficher
			$tbAfC[$rwrq[0]] = ($rwrq[1] != "" && $rwrq[1] != "HID"); // initialise tabeau des colonnes affichees
			$nmvarAfC = "AfC_".$NM_TABLE.sepNmTableNmChp.$rwrq[0];
			if (isset($_REQUEST[$nmvarAfC])) {// si cette var existe, colonne s�ectionnable
				$afcexists = true;
				// si affichage selectionnable ne tient pas compte de TYPAFF_L
				$tbAfC[$rwrq[0]] = ($_REQUEST[$nmvarAfC] == "yes"); //on MAJ le tableau tableau associatif
				$_SESSION['memFilt'][$nmvarAfC] = $_REQUEST[$nmvarAfC];
			}
		} // fin boucle sur les champs
		// ne renregistre que si les variables ont ete définie ou changées
		if ($condexists) $_SESSION["where_sup"] = $where_sup; 
		$where = ($where_sup=="" ? "" : "where ".$where_sup);
		$_SESSION['where'] = $where;
		unset($_SESSION['reqcust']);
		// fin si pas req custom
	} else { // req custom
		$reqcust = $_REQUEST['lc_reqcust'];
		$tblvarrqc = explode(",",$_REQUEST['tblvarrqc']); // liste des champs sur lesquels portent la requête
//		print_r($tblvarrqc);
		$condexists=false;
		$afcexists=false;
		$first = true;
		$nbv = 1;
		foreach ($tblvarrqc as $varreq) {
			list($NmTable,$NmChamp) = explode(sepNmTableNmChp,$varreq);
			$cond = retCondPya($_SESSION['parenv']['bdd_name'], $NmTable, $NmChamp);
			if (empty($cond)) $cond = 'TRUE';
			// on teste maintenant l'existence de variables de colonnes a afficher
			$nmvarAfC = "AfC_".$varreq;
			$tbAfC[$varreq] = true; // par défaut
			if (isset($_REQUEST[$nmvarAfC])) {// si cette var existe, colonne selectionnable
				$afcexists = true;
				// si affichage selectionnable ne tient pas compte de TYPAFF_L
				$tbAfC[$varreq] = ($_REQUEST[$nmvarAfC] == "yes"); //on MAJ le tableau tableau associatif
				$_SESSION['memFilt'][$nmvarAfC] = $_REQUEST[$nmvarAfC];
			}
			$nmvarNuVarrqc = "nvc_".$varreq; // N° de la variable
			$reqcust = str_replace("###$nbv###",$cond,$reqcust);
			$nbv ++;
		} // fin boucle sur les var de requete cust
		$_SESSION['reqcust'] = $reqcust;
		unset($_SESSION['where']);
	}
	// enregistre tableau des colonnes à afficher
	$_SESSION["tbAfC"] = $tbAfC; //session_register ("tbAfC");
} elseif (!isset($_SESSION["PgReq"])) $_SESSION["PgReq"] = 0;// fin si on dit recalculer la req

if ($NM_TABLE == "__reqcust") {
	$reqcust = $_SESSION['reqcust'];
	$result = db_query($reqcust);
	$nbrows = db_num_rows($result);
	$reqcust = addwherefORlimit($reqcust, $_SESSION['parenv']['nbligpp'], $_SESSION['FirstEnr']);
	$LB_TABLE = $_SESSION['reqcust_name'];
	$COM_TABLE = "";
	$TitreHP = $LB_TABLE;
	$EchWher = "<br><small>$reqcust</small>";
} else { // pas req cust
	/**
	REMARQUE SUR LES CLAUSES LIMIT

	AVEC ORACLE c'EST LE BORDEL MONSTRE
	Voir l'explication du comportement de ROWNUM là http://www.oracle.com/technology/oramag/oracle/06-sep/o56asktom.html
	Comme oracle compte les résultats au fur et à mesure, une req "select * from t where ROWNUM > 1;" ne ramene JAMAIS rien

	L'astuce (pourrie) trouvée : on appelle la requete sans condition basse et on affiche pas les $FirstEnr premiers résultats

	$GLOBALS["NmChpOid"] est le champs masqué qui sert de clé/identifiant d'enregistrement
	**/

	switch ($GLOBALS['db_type']) {
		case "mysql":
			$limitc =  " LIMIT ".$_SESSION['FirstEnr'].",".$_SESSION['parenv']['nbligpp'];
			break;
		
		case "pgsql":
			$limitc =  " OFFSET ".$_SESSION['FirstEnr']." LIMIT ".$_SESSION['parenv']['nbligpp'];
			break;

		case "oracle" :
			//$wherelimit = " (ROWNUM >= $FirstEnr AND ROWNUM <= ".($FirstEnr + $_SESSION['parenv']['nbligpp']).") ";
			$wherelimit = " (ROWNUM <= ".($_SESSION['FirstEnr'] + $_SESSION['parenv']['nbligpp']).") "; // voire "REMARQUE SUR LES CLAUSES LIMIT au début de ce fichier"
			break;
	}

	$where =  $_SESSION['where'];
	$EchWher = "<br><small>Condition: $where</small>";
	$nbrows =  db_count($GLOBALS['CSpIC'].$NM_TABLE.$GLOBALS['CSpIC'],$where);
	$where = $wherelimit!="" ? (trim($where)!="" ? "$where AND $wherelimit" : " where $wherelimit") : $where;
	$from = $GLOBALS['CSpIC'].$NM_TABLE.$GLOBALS['CSpIC'] ;
	$result = db_query("SELECT 1 FROM $from $where");
	$TitreHP = ($ss_parenv['ro']==true ? trad('com_consultation') : trad('com_edition')).trad('com_de_la_table'). $LB_TABLE;
}

echHtmlHeader(true);
$title=trad('LR_title'). $NM_TABLE." , ".trad('com_database')." ". $_SESSION['parenv']['bdd_name'];

?>
<a name="haut"></a>
<div align="center">
<?php if (!$_SESSION['parenv']['noinfos'])	echo '<H3>'.trad('com_database')." ".$_SESSION['parenv']['bdd_name'].'</H3>';
echo "<H1>$TitreHP</H1>";
if (!$_SESSION['parenv']['noinfos']) echo ($COM_TABLE!="" ? "$COM_TABLE" : ""); 
if (($where != "" || $NM_TABLE == "__reqcust") && !$_SESSION['parenv']['noinfos']) echo $EchWher;
// On affiche le resultat
if ($nbrows==0) { // Si nbre resultat = 0
	echo '<H3>'.trad('LR_no_record').'</H3>';
} else { // si nbre resultat>0
	$s = ($nbrows>1 ? "s" : ""); // accord pluriel
	?>
		<script language="JavaScript">
		// boite de confirmation  de suppression d'un enregistrement
		function ConfSuppr(url) {
		if (<?php echo ($NoConfSuppr!="No" ? "confirm('".trad('LR_confirm_del_message')."')": "true")?>)
		self.location.href=url;
		}
		</script>
	<?php 
	echo '<H3>'.$nbrows.trad('com_record').$s.trad('LR_to_list').'<H3>';
	echo '<H4>'.trad('LR_display_record').$s.'<em>'.($_SESSION['FirstEnr'] + 1)." ".trad('com_to')." ".min($nbrows,($_SESSION['FirstEnr'] + $_SESSION['parenv']['nbligpp'])).'</em></H4>';
	if (!$_SESSION['parenv']['ro']  && $NM_TABLE!="__reqcust") 
		echo '&nbsp;&nbsp;<a class="fxbutton" href="edit_table.php" title="'.trad('LT_addrecord').'"> <img src="media/new_r.gif">'.trad('LT_addrecord').'</a>'.nbsp(15);
	echo '<a href="#bas" class="fxbutton" title="'.trad('com_vers_enbas_bulle').'"><img src="media/flbas.png">'.trad('com_vers_enbas').'</a><br>';
	if ($orderb!="" && !$_SESSION['parenv']['noinfos'])	echo "<small>".str_replace ("ORDER BY", trad('LR_orderby'),$orderb)."</small><BR>";
	dispLastAdmiSql();
	?>
	<!--On affiche les colonnes qui correspondent aux champs selectionnées-->
	<TABLE>
	<THEAD>
	<TH>
	<?php echo (($_SESSION['parenv']['ro'] || $NM_TABLE == "__reqcust") ? trad('com_details') : trad('LR_del_mod_cop')); ?>
	</TH>
	<?php
	if ($NM_TABLE != "__reqcust") {
		$rq1 = db_query("select * from $TBDname where NM_TABLE='$NM_TABLE' AND NM_CHAMP!='$NmChDT' ORDER BY ORDAFF_L, LIBELLE");
		$nbcol = 0; // n de colonne
		while ($res0 = db_fetch_assoc($rq1)) {
			$tblallchps[] = $res0[$ult['NM_CHAMP']]; // on fait un tableau avec TOUS les champs à cause des clés multiples qui peuvent être allées chercher dans des champs masqués
			$tbobjCC[$nbcol] = $res0[$ult['NM_CHAMP']];
			if ($_SESSION["tbAfC"][$res0[$ult['NM_CHAMP']]]) {
				$nbcol++; // la condition n'est true que si champ a afficher et case cochee 
			}
		} // fins si $NM_TABLE != "__reqcust"
		$nbcol = ($nbcol-1);
		
		$tbCIL = array(); // reinitialise le tableau
		$t1 = microtime();
		for ($i=0;$i<=$nbcol;$i++) {
			$NomChamp = $tbobjCC[$i];
			$tbCIL[$NomChamp] = createOrLoadPyaObj($_SESSION['parenv']['bdd_name'], $NM_TABLE, $NomChamp);
			$tbCIL[$NomChamp]->DirEcho = false;
		} // fin boucle sur les champs
		// rajoute le champ rowid au début pour les éditions,etc...
		$lctd = ($GLOBALS['db_type'] == "oracle" ? "ROWIDTOCHAR(ROWID)," : ""). implode(",",$tblallchps);
		$from = $GLOBALS['CSpIC'].$NM_TABLE.$GLOBALS['CSpIC']  ;
		$reqcust = 'select '.$lctd.($GLOBALS["NmChpOid"]!="" ? ",".$GLOBALS["NmChpOid"] : "")." from $from";
	} else { // requete custom (perd l'ordre d'affichage sinon)
		$tbCIL = InitPOReq($reqcust,$_SESSION['parenv']['bdd_name'],false); // construction et initialisation du tableau d'objets
		/// TODO TODO il faut ds le cas d'oracle faire un espece de hash plutot en tableau assoc des rowid avec toutes les colonnes du from passé en param
		  ///$reqcust = ($GLOBALS['db_type'] == "oracle" ? str_ireplace("select","select ROWIDTOCHAR(sdm_saillie.ROWID),",$reqcust) : $reqcust);
	}
	
	$lb_orderasc = trad('LR_order_asc');
	$lb_orderdesc = trad('LR_order_desc');
	
	foreach($tbCIL as $kc=>$CIL) { if (is_object($CIL) && $kc != 'db_resreq') {
		$NomChamp = $CIL->NmChamp;
		
		if (($CIL->Typaff_l != "" && $CIL->Typaff_l != "HID") || ($_SESSION["tbAfC"][$NomChamp] && $NM_TABLE == "__reqcust") ) { // rajout a cause des req custom
			echo "<TH>";
			DispFlClasst($NomChamp); // affiche fleches de classement existant
			echo '<A HREF="list_table.php?chptri='.$NomChamp.'&ordtri=asc" title="'.$lb_orderasc.'"><IMG SRC="media/flasc.gif" alt=" [V] "></A>&nbsp;';
			echo '<A HREF="list_table.php?chptri='.$NomChamp.'&ordtri=desc" title="'.$lb_orderdesc.'"><IMG SRC="media/fldesc.gif" alt="[^]"></A>&nbsp;';
			echo "<BR>".$CIL->Libelle;
			echo "</TH>\n";
		} else unset ($tbCIL[$NomChamp]); // sinon efface l'objet
	}} // fin boucle sur les colonnes affichees
	?>
	</THEAD>
	<?php
	echo "<!--Req sql = $reqcust $where $orderb $limitc -->";
	$req = db_query("$reqcust $where $orderb $limitc"); // dans le cas
	/* pour la clé:
	- s'il y a une clé primaire, on la constitue;  ds ce cas $pk=true
	- sinon, la clé est constituée de tous les champs
	*/
	
	if ($GLOBALS['db_type'] == "mysql") {
		$nbpk = 0;  // nbre de champs cl� primaires
		for($Idf = 0; $Idf < db_num_fields($req); $Idf++) {
			/* DONE rendre compatible avec Mysqli
			//echo mysql_field_flags($req,$Idf)." <BR/>";
			if (stristr(mysql_field_flags($req, $Idf),"primary")) {
				$tbpk[$nbpk] = mysql_field_name($req, $Idf);
				*/
			$ff = mysqlivm_field_flags($req, $Idf);
			//echo "$Idf:";print_r($ff);
			if (stristr($ff,"primary")) {
				$tbpk[$nbpk] = db_field_name($req, $Idf);
				$nbpk++;
			} // fin si champ est une clé primaire
		}  // fin boucle sur les champs
	} // fin mysql
	//  $chp0=mysql_field_name($req,0);
	//  $chp1=mysql_field_name($req,1);
	//  if (mysql_num_fields($req)>2) $chp2=mysql_field_name($req,2);
	$nolig = $noligoracle = 0;
	$lb_recedit=trad("LR_record_edit");
	$lb_recdel=trad("LR_record_delete");
	$lb_reccopy=trad("LR_record_copy");
	$lb_recshow=trad("LR_record_show");
	
	// 
	while ($tbValChp=db_fetch_assoc($req)) {
		$noligoracle++;
		if ($GLOBALS['db_type'] != "oracle" || $noligoracle >= $_SESSION['FirstEnr']) {  // voire "REMARQUE SUR LES CLAUSES LIMIT au début de ce fichier"
			$nolig++;
			// premiere colonne: modifier / supprimer
			echo "<TR class=\"".($nolig % 2 == 1 ? "backwhiten" : "backredc")."\"><TD class=\"LRcoledit\" align=\"center\">";
			// gestion clé
			$key = ""; // reinit
			if ($GLOBALS['db_type'] == "mysql") {
				if ($nbpk > 0) { // cle primaire existe : on la construit (elle peut être multiple)
					for ($Idf = 0; $Idf < $nbpk; $Idf++)
						$key .= $tbpk[$Idf]."='".$tbValChp[$tbpk[$Idf]]."' AND ";
				} else { // pas de clé primaire: on prend ts les champs
					foreach ($tbValChp as $Chp=>$Val) $key.="$Chp='$Val' AND ";  // for ($Idf=0;$Idf<mysql_num_fields($req);$Idf++) $key.=mysql_field_name($req,$Idf)."='".$tbValChp[$Idf]."' AND ";
				}
				$key = vdc($key,5); // enlève le dernier " AND "
			} elseif ($GLOBALS['db_type'] == "pgsql") {
				$key = "oid=".$tbValChp['oid'];
			} elseif ($GLOBALS['db_type'] == "oracle" &&  $NM_TABLE != "__reqcust"){ /// TODO pour l'instant on gère pas l'affichage fiche en req custom
				$key = "ROWIDTOCHAR(ROWID)='".$tbValChp['ROWIDTOCHAR(ROWID)']."'";
			}
			$url = urlencode(addslashes("amact_table.php?key=".$key."&modif=-1&SESSION_NAME=".$_REQUEST['SESSION_NAME']));
			//die($url);
			if (!$_SESSION['parenv']['ro'] && $NM_TABLE != "__reqcust") { // bouton supprimer et duppliquer que quand read only false ou req custom
				echo "<A HREF=\"javascript:ConfSuppr('".$url."');\" TITLE=\"$lb_recdel\"><IMG SRC=\"media/del.png\"  alt=\" [X] \"></A>";
				echo "<A HREF=\"edit_table.php?key=".$key."&modif=1\" TITLE=\"$lb_recedit\"><IMG SRC=\"media/edit.png\" alt=\" Edit \"></A>";
				echo "<A HREF=\"edit_table.php?key=".$key."&modif=2\" TITLE=\"$lb_reccopy\"><IMG SRC=\"media/copie.png\" alt=\" Copy \"></A>";
			}
			//else { // affichage en read only (loupe et d�ails de l'enregistrement)
			echo "<A HREF=\"edit_table.php?key=".$key."&modif=C\"  TITLE=\"$lb_recshow\" ><IMG SRC=\"media/eye-open.png\"></A>";
			//}
			echo "</TD>\n";
			// colonnes suivantes
			foreach ($tbCIL as $kc=>$objCIL) { if (is_object($objCIL) && $kc != 'db_resreq') { // boucle sur le tableau d'objets colonnes
				echo "<TD>";
				$NomChamp = $objCIL->NmChamp;
				$objCIL->AffVal($tbValChp); // affecte valeur; un peu traps dans le cas des clés multiple
				if (VerifAdMail($tbValChp[$NomChamp])) $MailATous .= $tbValChp[$NomChamp].",";
				echo $objCIL->EchoVCL(); // affiche Valeur Champ ds Liste
				echo "</TD>\n";
			}}  // fin boucle sur les colonnes
			echo "</TR>\n";
		} // fin si on affiche les lignes d'avant Oracle
	} // fin while = fin boucle sur les lignes
} // fin si nbrows>0
?>
<br>
</table>
<a name="bas">
<br>
<?php
if (!$_SESSION['parenv']['ro'] && $NM_TABLE != "__reqcust") echo '<a class="fxbutton" href="edit_table.php" title="'.trad('LT_addrecord').'"> <img src="media/new_r.gif"> '.trad('LT_addrecord').'</a>'.nbsp(15);
echo '<a href="#haut" class="fxbutton" title="'.trad('com_vers_enhaut_bulle').'"><img src="media/flhaut.png"> '.trad('com_vers_enhaut').'</a>';

if ($_SESSION['FirstEnr'] > 0) 
	echo "&nbsp;&nbsp;&nbsp;<A class=\"fxbutton\" style=\"padding-top: 7px\" HREF=\"list_table.php?lc_FirstEnr=".max(0,$_SESSION['FirstEnr'] - $_SESSION['parenv']['nbligpp'])."\" title=\"".trad('LT_display_the').$_SESSION['parenv']['nbligpp'].trad('LT_preced_recs')."\"> <img src=\"media/preced.png\" border=\"0\"> </A>&nbsp;&nbsp;&nbsp;";

if (($_SESSION['FirstEnr'] + $_SESSION['parenv']['nbligpp']) < $nbrows) 
	echo '&nbsp;&nbsp;&nbsp;<A class="fxbutton" style="padding-top: 7px"  HREF="list_table.php?lc_FirstEnr='.($_SESSION['FirstEnr'] + $_SESSION['parenv']['nbligpp']).'" title="'.trad('LT_display_the').$_SESSION['parenv']['nbligpp'].trad('LT_follow_recs').'"> <img src="media/suivant.png" border="0"> </A>';
//echo '&nbsp;&nbsp;&nbsp;'.trad(LT_nblig_aff_ppage).' <input type="text" name="lc_nbligpp" size="3" maxlength="3" value="'.($_SESSION['parenv']['nbligpp']>0 ? $_SESSION['parenv']['nbligpp'] : $nbligpp_def).'">';
//echo '<br><br>'.ret_adrr($_SERVER["PHP_SELF"],true);
echo '<br><br>';
if ($_SESSION["NM_TABLE"] == "__reqcust") echo '<a href="main.php" class="fxbutton">'.trad('BT_retour_acceuil').'</a>&nbsp;&nbsp;&nbsp;';
if ($_SESSION["PgReq"] > 0) echo '<a href="req_table.php?cflt=1" class="fxbutton">'.trad('BT_retour').'</a>';

//if ($PgReq==1) 
//	echo '&nbsp;&nbsp;&nbsp;<a class="fxbutton" title="'.trad('LR_query_back_bulle').'" href="req_table.php?lc_NM_TABLE='.$NM_TABLE.'"> '.trad('LR_query_back').'</a>';
if ($nbrows>0) {
	echo '<a class="fxbutton" href="extraction.php?whodb='.urlencode($where." ".$orderb).'&lc_NM_TABLE='.$NM_TABLE.'" title="'.trad('LR_download_bulle').'"><img src="media/filesave.png"> '.trad('LR_download').'</A> &nbsp;&nbsp;&nbsp; 
	<a class="fxbutton" href="extraction.php?encod=iso&whodb='.urlencode($where." ".$orderb).'" title="'.trad('LR_download_bulle').' EN ISO FORMAT"> <img src="media/filesave.png">Extract. ISO</A>';
	if ($MailATous!="") {
		$MailATous = vdc($MailATous,1); // dégage la derniere virgule
		echo '&nbsp;&nbsp;&nbsp;<a href="mailto:'.$MailATous.'" title="'.trad('LR_mail_to_all_bulle').'"><img src="media/mail_send.png"> '.trad('LR_mail_to_all').'</A>';	
	} // fin MailATous
} // fin nblig>0
?>

</div>
<?php 
echHtmlFooter('nav');


/**
 * 
 * Affichage des flêches de classeùent
 * @param unknown_type $NmChamp
 */
function DispFlClasst($NmChamp) { // affiche fleches de classement
	for ($l=1;$l<=3;$l++) {
	if ($_SESSION['tbchptri'][$l]==$NmChamp) {
		echo "<IMG SRC=\"media/fl".$l.$_SESSION['tbordtri'][$l].".gif\" alt=\"la flèche indique le sens et le n° d'ordre de clé du classement de ce champ\">&nbsp;";
		break;
	}
  }
}
