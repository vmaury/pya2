<?php
// utilitaire permettant de CONSULTER la table DESC_TABLE 
include_once ('includes/config.inc.php');
// session_start_wthspid();
checkSessDBC();
echHtmlHeader(true);

?>
<H1>Super Administration de phpYourAdmin</H1>
<H2>Définitions complètes de la base <?php echo$_SESSION['parenv']['bdd_name']?></H2>
<TABLE>
<?php
$rqT = db_query("select * from $TBDname where NM_CHAMP='$NmChDT' ORDER BY ORDAFF_L,NM_TABLE");
while ($rpT = db_fetch_array($rqT)) {
      echothead($_REQUEST['simpl']);
      $nolig = 0;
      echo "<tr><td><H3>".$rpT['NM_TABLE']."</h3></td><td><b>".$rpT['LIBELLE']."</b>&nbsp;</td>";
      if ($_REQUEST['simpl']!=1) {
      	echo "<td>".$rpT['ORDAFF_L'].'&nbsp;</td><td colspan="8">';
      } else echo '<td colspan="2">';
      echo $rpT['COMMENT']."&nbsp;</td></tr>\n";
      // recup les  caract. des champs
    
      $table_def = db_table_defs($rpT['NM_TABLE']);

      $rqC = db_query("select * from $TBDname where NM_CHAMP!='$NmChDT' AND NM_TABLE='".$rpT['NM_TABLE']."' ORDER BY ORDAFF,NM_CHAMP");
      while ($rpC = db_fetch_assoc($rqC)) {
            $nolig++;
            echo "<TR>";
            echo "<td><b>".$rpC['NM_CHAMP']."</b>";
            $NM_CHAMP = $rpC['NM_CHAMP'];
            echo "<BR><small>".$table_def[$NM_CHAMP]['FieldType']."&nbsp;; ".$table_def[$NM_CHAMP]['FieldValDef']."&nbsp;; ".$table_def[$NM_CHAMP]['FieldNullOk']."&nbsp;;".$table_def[$NM_CHAMP]['FieldKey']."&nbsp;; ".$table_def[$NM_CHAMP]['FieldExtra']."</small>\n";
	     // auto
            echo "</td>";
            echo "<td>".$rpC['LIBELLE']."&nbsp;</td>";
            if ($_REQUEST['simpl']!=1) {
                 echo "<td>".$rpC['TYPAFF_L']." (".$rpC['ORDAFF_L'].")&nbsp;</td>";
                 echo "<td>".$rpC['TYPEAFF']." (".$rpC['ORDAFF'].")&nbsp;</td>"; 
            }
            echo "<td>".$rpC['VALEURS']."&nbsp;</td>";
            if ($_REQUEST['simpl']!=1) { 
               echo "<td>".$rpC['VAL_DEFAUT']."&nbsp;</td>";
               echo "<td>".$rpC['TYP_CHP']."&nbsp;</td>";
               echo "<td>".$rpC['TT_AVMAJ']."&nbsp;</td>";
               echo "<td>".$rpC['TT_PDTMAJ']."&nbsp;</td>";
               echo "<td>".$rpC['TT_APRMAJ']."&nbsp;</td>";
            }
            echo "<td>".$rpC['COMMENT']."&nbsp;</td></tr>\n";
      } // fin boucle sur les champs
   }
?>
</table>
<input class="fxbutton" type="submit" value="<< RETOUR" onclick="history.back()">

<?php 
echHtmlFooter('nav');


function echothead($simpl) {
?>
<thead>
<th>TABLE/CHAMP<BR><small>Type&nbsp;; Val. déf.&nbsp;; Null OK&nbsp;; Clé/index&nbsp;; Extra</small></th><th>LIBELLE</th>
<?php if ($simpl!=1) { ?>
   <th>Aff. etat</th><th>Aff. Edit</th>
   <?php } ?>
<th>VALEURS</th>
<?php if ($simpl!=1) { ?>
<th>Filtre</th><th>AFS</th><th>TT_AVMAJ</th><th>TT_PDTMAJ</th><th>TT_APRMAJ</th>
<?php } ?>
<th>Commentaire</th>
</thead>
<?php }
