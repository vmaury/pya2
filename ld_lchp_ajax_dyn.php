<?php // liste deroulante de liste des champs d'une table
// appellee par les popl en ajax..

/**
NM_TABLE : nom de la table
NM_CHPS : nom du ou des champs sélectionnés
NMID : Nom et If htm
MULT : ld multiple ou pas
FIRSTEMPTY : rajoute une premiere option o/n
*/
include_once ('includes/config.inc.php');
// session_start_wthspid();
checkSessDBC();
//echHtmlHeader(true);

$charset = ($_SESSION['ss_parenv']['encoding']!="" ? $_SESSION['ss_parenv']['encoding'] : "utf-8");
@ini_set("default_charset", $charset);
header('Content-type: text/html; charset='.$charset);

$resf = db_query(addwherefORlimit("select * from ".$_REQUEST['NMTABLE'],1)); // uniquement pour avoir la liste des champs
$nfields = db_num_fields($resf);

if ($_REQUEST["NM_CHPS"]) {
	$tbnmchp = explode(":",	$_REQUEST["NM_CHPS"]);
} else $tbnmchp = array();
if ($nfields>0) { //'.($_REQUEST['MULT']>0 ? 'multiple="multiple" size="8"' : '').'
	echo '<select name="'.$_REQUEST['NMID'].'" id="'.$_REQUEST['NMID'].'" '.($_REQUEST['MULT']>0 ?  'multiple="multiple" size="8"' : '').'">';
	if ($_REQUEST['FIRSTEMPTY']) echo '<OPTION value=""></OPTION>';
	for ($j = 0; $j < $nfields; $j++) {
		$sel = in_array(db_field_name ($resf, $j),$tbnmchp) ? ' selected="selected" ' : '';
		echo '<OPTION value="'.db_field_name ($resf, $j).'" '.$sel.'>'.db_field_name ($resf, $j).'</OPTION>';
		}
	echo "</select>";
} else echo "Aucun champ dans la table ".$_REQUEST['NMTABLE']." correspondant aux criteres..";

?>
