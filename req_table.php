<?php 
include_once ('includes/config.inc.php');
//checkSessDBC();

//print_r($_SESSION);
$ult = rtb_ultchp(); // tableau des noms de champs sensibles a la casse (a cause de pgsql...)

$_SESSION['FirstEnr'] = 0;
if (isset($_REQUEST['lc_where_sup'])) {
	$_SESSION["where_sup"] = $_REQUEST['lc_where_sup'];
}
if ($_REQUEST['lc_NM_TABLE']) $_SESSION['NM_TABLE'] = $_REQUEST['lc_NM_TABLE'];
//print_r($_REQUEST);
if ($_REQUEST['reqcust_key']) {
	$creq = db_qr_rass2 ("select * from $TBDname where NM_CHAMP='".$_REQUEST['reqcust_key']."' AND NM_TABLE='__reqcust'");
	$_SESSION["reqcust_name"]  = $creq['LIBELLE'];
	$_SESSION["lc_reqcust"]  = $_REQUEST["lc_reqcust"] = $creq[$GLOBALS["NmChpComment"]];
	$_REQUEST['lc_NM_TABLE'] = "__reqcust";
	
} else {
	if ($_REQUEST['reqcust_name']!= "") $_SESSION["reqcust_name"] = $_REQUEST['reqcust_name'];
	if ($_REQUEST['lc_reqcust']!= "")  $_SESSION["lc_reqcust"] = $_REQUEST['lc_reqcust'];
}

$reqcust = $_SESSION["lc_reqcust"];
if ($_REQUEST['cflt'] != 1) { // on vient pas d'un retour de la liste
	// reset des variables de session de tri, d'ordre, d'enregistrement de debut et d'affichage des colonnes
	unset($_SESSION["tbchptri"]); 
	unset($_SESSION["tbordtri"]); 
	$_SESSION["FirstEnr"] = 0;
	unset($_SESSION["tbAfC"]);
} else {
	$_REQUEST['lc_NM_TABLE'] = $_SESSION['NM_TABLE'];
} 
// regarde s'il existe des filtres ou selection d'affichage de colonnes, que si pas de req custom
if ($_REQUEST['lc_NM_TABLE'] != "__reqcust") {
	$sqlqr = "SELECT NM_CHAMP from $TBDname where NM_CHAMP!='$NmChDT' AND NM_TABLE='".$_REQUEST['lc_NM_TABLE']."' AND (VAL_DEFAUT".$GLOBALS['sqllenstr0']."  OR TYP_CHP".$GLOBALS['sqllenstr0'].") AND TYPAFF_L".$GLOBALS['sqllenstr0']." order by ORDAFF_L, LIBELLE";
	//die($sqlqr);
   $qr = db_query($sqlqr) ; // recupere libelle, ordre affichage et COMMENT, si type affichage ="HID", on affiche pas la table
   $nbrqr = db_num_rows($qr);
   
} else {
	$tbargscust = parseArgsReq($reqcust);
	$nbrqr = count($tbargscust);
}
// sinon, va directement sur la liste de réponses
if ($nbrqr==0) {
	$url = "list_table.php?SESSION_NAME=".$_REQUEST['SESSION_NAME']."&lc_NM_TABLE=".$_REQUEST['lc_NM_TABLE']."&lc_where_sup=".urlencode($_SESSION["where_sup"])."&lc_PgReq=0&lc_reqcust=".urlencode($_REQUEST['lc_reqcust']); //."&cfopl=".$_REQUEST['cfopl']
	//die($url);
	outJS("window.location.replace('$url')",true) ;
	//header("location: $url");
  	die();
}

$title = trad('REQ_query_on_table').$_REQUEST['lc_NM_TABLE']." , ".trad('COM_database').$_SESSION['parenv']['bdd_name'];
echHtmlHeader(true);
JSpopup(); // sert pour la nouvelle popup de filtre
?>
<div align="center">
<form action="list_table.php" method="post" name="theform" ENCTYPE="multipart/form-data">
<input type="hidden" name="lc_where_sup" value="<?php echo $_SESSION["where_sup"]?>">
<input type="hidden" name="lc_PgReq" value="1">
<?php if (!$_SESSION['parenv']['noinfos']) { 
	//echo var_export($_SESSION['memFilt']);
	echo "<H3>".strtoupper(trad('com_database').$_SESSION['parenv']['bdd_name'])."</H3>";
}
if (!$tbargscust) { // pas requête Custom
	?>
	<H1><?php echo trad('REQ_crit_select').$_REQUEST['lc_NM_TABLE'];?></H1>
	<P><?php echo "&nbsp;&nbsp;<a class=\"fxsmallbutton\" href=\"req_table.php?clearCrit=true&lc_NM_TABLE=".$_REQUEST['lc_NM_TABLE']."\">". trad("REQ_clean_memFilt")."</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	if ($_REQUEST['clearCrit']) unset($_SESSION['memFilt']);
	if (!$_SESSION['parenv']['ro']) { 
		echo "&nbsp;&nbsp;<a class=\"fxsmallbutton\" href=\"edit_table.php?lc_NM_TABLE=".$_REQUEST['lc_NM_TABLE']."&cfp=".$_SERVER["PHP_SELF"]."\" title=\"".trad('LT_addrecord')."\"> <img src=\"media/new_r.gif\"> ".trad('LT_addrecord')." </a>";
	}
	dispLastAdmiSql();
	?>
	</P>
	<input type="hidden" name="lc_NM_TABLE" value="<?php echo $_REQUEST['lc_NM_TABLE'];?>">
	<TABLE>
	<THEAD>
	<TH>Champ</TH><TH>Critere</TH><TH>A Afficher</TH>
	</THEAD>
	<?php
	$nolig = 0;
	while ($res=db_fetch_array($qr)) {
		$nolig++;
		$FCobj = createOrLoadPyaObj($_SESSION['parenv']['bdd_name'], $_REQUEST['lc_NM_TABLE'], $res[$ult['NM_CHAMP']]);
		$FCobj->DirEcho = true;
		echo "<TR><TD>$FCobj->Libelle".($FCobj->Comment != '' ? "<BR><small>$FCobj->Comment</small>" : "")."</TD><TD>";
		$FCobj->EchoFilt();
		echo "</TD><TD>";
		$FCobj->EchoCSA();
		echo "</TD></TR>\n";
	}
} else { // req custom
	echo "<H1>".trad("REQ_crit_req_cust").$_SESSION["reqcust_name"]."</H1>";
	echo "<p>&nbsp;&nbsp;<a class=\"fxsmallbutton\" href=\"req_table.php?clearCrit=true&lc_NM_TABLE=".$_REQUEST['lc_NM_TABLE']."\">". trad("REQ_clean_memFilt")."</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	if ($_REQUEST['clearCrit']) unset($_SESSION['memFilt']);
	unset($_SESSION['tbPO4ReqCust']);
	//print_r($tbargscust);
	?></P>
	<input type="hidden" name="lc_NM_TABLE" value="__reqcust">
	<input type="hidden" name="lc_reqcust" value="<?php echo $reqcust?>">
	<TABLE>
	<TR class="THEAD">
	<TH>Paramètre</TH><TH>Valeur</TH><TH>Commentaire</TH></TR>
	<?php
	$nolig=0;
	foreach ($tbargscust as $arg) {
		//$arg => Array ( [0] => llx_assetOf#numero [1] => Filt [2] => [3] => )
		// $tbpropPya = hash_explode($arg);
		list($NmTable,$NmChamp) = explode(sepNmTableNmChp,$arg[0]);
		$nolig++;
		// pour l'instant on traite que le cas des filtres PYAObj
		$tblvarrqc[] = $arg[0];
		$FCobj = createOrLoadPyaObj($_SESSION['parenv']['bdd_name'], $NmTable, $NmChamp, true);
		echo "<TR><TD><B>$FCobj->Libelle</B><BR><small>$FCobj->Comment</small></TD><TD>";
		$FCobj->EchoFilt();
		echo '<input type="hidden" name="nvc_'.$NmChamp.'" value="'.$nolig.'">';
		echo "</TD><TD>";
		$FCobj->EchoCSA();
		echo "</TD></TR>\n";
	}
	echo '<input type="hidden" name="tblvarrqc" value="'.implode(",",$tblvarrqc).'">';

	
}
?>
<tr><td colspan="3" align="center"><br>
<input type="submit" value="<?php echo trad('BT_valider')?>" class="fxbutton"/> 
</td></tr>
</TABLE>
</form>
<?php if (!$_SESSION['parenv']['dispInIframe']) { ?><a href="main.php"  class="fxbutton">Retour</a> <?php }?> 
</div>
<?php 
echHtmlFooter('nav');
